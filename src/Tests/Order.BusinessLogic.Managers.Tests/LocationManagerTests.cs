﻿using Microsoft.Extensions.Logging;
using Moq;
using Order.BusinessLogic.Exceptions;
using Order.BusinessLogic.Managers.Abstraction;
using Order.BusinessLogic.Models;
using Order.DataAccess.Models;
using Order.DataAccess.Repositories.Abstraction;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Order.BusinessLogic.Managers.Tests
{
    public partial class LocationManagerTests
    {
        private readonly ILocationManager _manager;
        private readonly Mock<IPreOrderRepository> _preOrderRepositoryMock;
        private readonly Mock<ILogger<LocationManager>> _loggerMock;

        public LocationManagerTests()
        {
            _preOrderRepositoryMock = new Mock<IPreOrderRepository>();
            _loggerMock = new Mock<ILogger<LocationManager>>();
            _manager = new LocationManager(_preOrderRepositoryMock.Object, _loggerMock.Object);
        }

        [Fact]
        public async Task FindNearbyAsync_Should_Throw_Exception_When_InnerDto_NullAsync()
        {
            // Arrange

            // Act
            var exception = await Assert.ThrowsAsync<BusinessLogicArgumentNullException>(async () => await _manager.FindNearbyAsync(null));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal($"Model {nameof(FindNearbyLocationDto)} is empty", exception.Message);
        }

        [Fact]
        public async Task FindNearbyAsync_Should_Throw_Exception_When_LocationFromSearch_NullAsync()
        {
            // Arrange
            var dto = new FindNearbyLocationDto
            {
                LocationFromSearch = null
            };

            // Act
            var exception = await Assert.ThrowsAsync<ArgumentNullException>(async () => await _manager.FindNearbyAsync(dto));

            // Assert
            Assert.NotNull(exception);
        }

        [Fact]
        public async Task FindNearbyAsync_Should_Throw_Exception_When_LocationFromSearch_GeoCoordinate_NullAsync()
        {
            // Arrange
            var dto = new FindNearbyLocationDto
            {
                LocationFromSearch = new LocationWithGetCoordinateDto
                {
                    GeoCoordinate = null
                }
            };

            // Act
            var exception = await Assert.ThrowsAsync<ArgumentNullException>(async () => await _manager.FindNearbyAsync(dto));

            // Assert
            Assert.NotNull(exception);
        }

        [Fact]
        public async Task FindNearbyAsync_Should_Throw_Exception_When_SearchInLocation_UnknownAsync()
        {
            // Arrange
            var dto = new FindNearbyLocationDto
            {
                LocationFromSearch = new LocationWithGetCoordinateDto
                {
                    GeoCoordinate = new GeoCoordinatePortable.GeoCoordinate()
                },
                SearchInLocation = SearchLocationEnum.Unknown
            };

            // Act
            var exception = await Assert.ThrowsAsync<Exception>(async () => await _manager.FindNearbyAsync(dto));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal("Please set search location", exception.Message);
        }

        [Fact]
        public async Task FindNearbyAsync_Should_Throw_Exception_When_UserId_NullAsync()
        {
            // Arrange
            var dto = new FindNearbyLocationDto
            {
                LocationFromSearch = new LocationWithGetCoordinateDto
                {
                    GeoCoordinate = new GeoCoordinatePortable.GeoCoordinate()
                },
                SearchInLocation = SearchLocationEnum.LocationFrom,
                UserId = null
            };

            // Act
            var exception = await Assert.ThrowsAsync<Exception>(async () => await _manager.FindNearbyAsync(dto));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal("UserId is not defined", exception.Message);
        }

        [Fact]
        public async Task FindNearbyAsync_Should_Return_EmptyArray_When_NoPreOrders_Async()
        {
            // Arrange
            var userId = Guid.NewGuid().ToString();
            var dto = new FindNearbyLocationDto
            {
                LocationFromSearch = new LocationWithGetCoordinateDto
                {
                    GeoCoordinate = new GeoCoordinatePortable.GeoCoordinate()
                },
                SearchInLocation = SearchLocationEnum.LocationFrom,
                UserId = userId
            };

            // Act
            var results = await _manager.FindNearbyAsync(dto);

            // Assert
            Assert.NotNull(results);
            Assert.Empty(results);
        }

        [Fact]
        public async Task FindNearbyAsync_Should_Return_EmptyArray_When_NoPreOrders2_Async()
        {
            // Arrange
            var userId = Guid.NewGuid().ToString();
            var dto = new FindNearbyLocationDto
            {
                LocationFromSearch = new LocationWithGetCoordinateDto
                {
                    GeoCoordinate = new GeoCoordinatePortable.GeoCoordinate()
                },
                SearchInLocation = SearchLocationEnum.LocationFrom,
                UserId = userId
            };
            _preOrderRepositoryMock
                .Setup(x => x.GetAllByDateMoreThan(It.IsAny<DateTime>(), userId))
                .ReturnsAsync((ICollection<PreOrder>)null);

            // Act
            var results = await _manager.FindNearbyAsync(dto);

            // Assert
            Assert.NotNull(results);
            Assert.Empty(results);
            _preOrderRepositoryMock
                .Verify(x => x.GetAllByDateMoreThan(It.IsAny<DateTime>(), userId), Times.Once);
        }

        [Fact]
        public async Task FindNearbyAsync_Should_Return_EmptyArray_When_NoPreOrders3_Async()
        {
            // Arrange
            var userId = Guid.NewGuid().ToString();
            var dto = new FindNearbyLocationDto
            {
                LocationFromSearch = new LocationWithGetCoordinateDto
                {
                    GeoCoordinate = new GeoCoordinatePortable.GeoCoordinate()
                },
                SearchInLocation = SearchLocationEnum.LocationFrom,
                UserId = userId
            };
            _preOrderRepositoryMock
                .Setup(x => x.GetAllByDateMoreThan(It.IsAny<DateTime>(), userId))
                .ReturnsAsync(new List<PreOrder>());

            // Act
            var results = await _manager.FindNearbyAsync(dto);

            // Assert
            Assert.NotNull(results);
            Assert.Empty(results);
            _preOrderRepositoryMock
                .Verify(x => x.GetAllByDateMoreThan(It.IsAny<DateTime>(), userId), Times.Once);
        }

        [Theory]
        [InlineData(SearchLocationEnum.LocationFrom, 1)]
        [InlineData(SearchLocationEnum.LocationTo, 1)]
        [InlineData(SearchLocationEnum.LocationFromAndTo, 2)]
        public async Task FindNearbyAsync_Should_Return_EmptyArray_When_PreOrders_NotFound_Location_Async(SearchLocationEnum searchLocation, int expectedResultCount)
        {
            // Arrange
            var userId = Guid.NewGuid().ToString();
            var dto = new FindNearbyLocationDto
            {
                LocationFromSearch = new LocationWithGetCoordinateDto
                {
                    GeoCoordinate = new GeoCoordinatePortable.GeoCoordinate(1, 1)
                },
                SearchInLocation = searchLocation,
                UserId = userId
            };
            _preOrderRepositoryMock
                .Setup(x => x.GetAllByDateMoreThan(It.IsAny<DateTime>(), userId))
                .ReturnsAsync(new List<PreOrder>
                {
                    new PreOrder
                    {
                        LocationFrom = new Location
                        {
                            Lat = 1,
                            Lng = 1,
                            Address = Guid.NewGuid().ToString()
                        },
                        LocationTo = new Location
                        {
                            Lat = 1,
                            Lng = 1,
                            Address = Guid.NewGuid().ToString()
                        }
                    }
                });

            // Act
            var results = await _manager.FindNearbyAsync(dto);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(expectedResultCount, results.Count);
            _preOrderRepositoryMock
                .Verify(x => x.GetAllByDateMoreThan(It.IsAny<DateTime>(), userId), Times.Once);
        }
    }
}
