﻿using System;
using System.Threading.Tasks;
using Order.BusinessLogic.Exceptions;
using Order.BusinessLogic.Models;
using Xunit;

namespace Order.BusinessLogic.Managers.Tests.PreOrderManager
{
    public partial class PreOrderManagerTests
    {
        [Fact]
        public async Task CreateMethod_Null_Dto_Should_Throw_BusinessLogicArgumentNullException()
        {
            // Arrange

            // Act
            var exception = await Assert.ThrowsAsync<BusinessLogicArgumentNullException>(async () => await _manager.CreateAsync(null, Token));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal($"Model {nameof(PreOrderCreateDto)} is empty", exception.Message);
        }

        [Fact]
        public async Task CreateMethod_CorrectResult_When_EmptyModel()
        {
            // Arrange
            var currentDateTime = DateTime.UtcNow;

            // Act
            var preOrderCreateResultDto = await _manager.CreateAsync(new PreOrderCreateDto(), Token);

            // Assert
            Assert.NotNull(preOrderCreateResultDto);
            Assert.False(preOrderCreateResultDto.Id == Guid.Empty);
            Assert.False(preOrderCreateResultDto.IsAuthorized);
            Assert.False(preOrderCreateResultDto.IsDeleted);
            Assert.False(preOrderCreateResultDto.IsOrderCreated);
            Assert.True(preOrderCreateResultDto.Created >= currentDateTime);
            Assert.Null(preOrderCreateResultDto.LocationFrom);
            Assert.Null(preOrderCreateResultDto.LocationTo);
            Assert.Null(preOrderCreateResultDto.Modified);
            Assert.Null(preOrderCreateResultDto.CreatedUserId);
        }

        [Fact]
        public async Task CreateMethod_CorrectResult_When_NotEmpty_Model()
        {
            // Arrange
            var currentDateTime = DateTime.UtcNow;
            var locationFromLng = 1;
            var locationFromLat = 1;
            var locationFromAddress = "1";
            var locationToLng = 2;
            var locationToLat = 2;
            var locationToAddress = "2";
            var createdUserId = "user";

            // Act
            var preOrderCreateResultDto = await _manager.CreateAsync(new PreOrderCreateDto
            {
                LocationFrom = new LocationDto
                {
                    Lat = locationFromLat,
                    Lng = locationFromLng,
                    Address = locationFromAddress
                },
                LocationTo = new LocationDto
                {
                    Lat = locationToLat,
                    Lng = locationToLng,
                    Address = locationToAddress
                },
                IsAuthorized = true,
                CreatedUserId = createdUserId
            }, Token);

            // Assert
            Assert.NotNull(preOrderCreateResultDto);
            Assert.False(preOrderCreateResultDto.Id == Guid.Empty);
            Assert.True(preOrderCreateResultDto.IsAuthorized);
            Assert.False(preOrderCreateResultDto.IsDeleted);
            Assert.False(preOrderCreateResultDto.IsOrderCreated);
            Assert.True(preOrderCreateResultDto.Created >= currentDateTime);

            Assert.NotNull(preOrderCreateResultDto.LocationFrom);
            Assert.Equal(locationFromLat, preOrderCreateResultDto.LocationFrom.Lat);
            Assert.Equal(locationFromLng, preOrderCreateResultDto.LocationFrom.Lng);
            Assert.Equal(locationFromAddress, preOrderCreateResultDto.LocationFrom.Address);

            Assert.NotNull(preOrderCreateResultDto.LocationTo);
            Assert.Equal(locationToLat, preOrderCreateResultDto.LocationTo.Lat);
            Assert.Equal(locationToLng, preOrderCreateResultDto.LocationTo.Lng);
            Assert.Equal(locationToAddress, preOrderCreateResultDto.LocationTo.Address);

            Assert.Null(preOrderCreateResultDto.Modified);
            Assert.Equal(createdUserId, preOrderCreateResultDto.CreatedUserId);
        }
    }
}
