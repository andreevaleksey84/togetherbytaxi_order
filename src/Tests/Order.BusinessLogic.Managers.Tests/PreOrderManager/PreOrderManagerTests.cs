using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using Order.BusinessLogic.Managers.Abstraction;
using Order.DataAccess.Repositories.Abstraction;
using Xunit;

namespace Order.BusinessLogic.Managers.Tests.PreOrderManager
{
    public partial class PreOrderManagerTests : BaseUnitTests, IClassFixture<Fixture>
    {
        private readonly IPreOrderManager _manager;
        private readonly IMainUnitOfWork _mainUnitOfWork;
        private readonly Mock<ILogger<Managers.PreOrderManager>> _preOrderManagerLoggerMock = new Mock<ILogger<Managers.PreOrderManager>>();
        private readonly IMapper _mapper;

        public PreOrderManagerTests(Fixture fixture)
        {
            _mapper = fixture.ServiceProvider.GetService<IMapper>();
            _mainUnitOfWork =  fixture.ServiceProvider.GetService<IMainUnitOfWork>();

            _manager = new Managers.PreOrderManager(_mainUnitOfWork, _mapper, _preOrderManagerLoggerMock.Object);
        }
    }
}