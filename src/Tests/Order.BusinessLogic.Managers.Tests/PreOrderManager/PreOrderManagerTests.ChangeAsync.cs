﻿using System;
using System.Threading.Tasks;
using Order.BusinessLogic.Exceptions;
using Order.BusinessLogic.Models;
using Xunit;

namespace Order.BusinessLogic.Managers.Tests.PreOrderManager
{
    public partial class PreOrderManagerTests
    {
        [Fact]
        public async Task EditMethod_Null_Dto_Should_Throw_BusinessLogicArgumentNullException()
        {
            // Arrange

            // Act
            var exception = await Assert.ThrowsAsync<BusinessLogicArgumentNullException>(async () => await _manager.ChangeAsync(null, Token));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal($"Model {nameof(PreOrderChangeDto)} is empty", exception.Message);
        }

        [Fact]
        public async Task EditMethod_Throws_Exception_When_UniqId_Not_Found()
        {
            // Arrange
            var changeDto = new PreOrderChangeDto {UniqId = Guid.NewGuid()};

            // Act
            var exception =
                await Assert.ThrowsAsync<EntityNotFoundException>(async () => await _manager.ChangeAsync(changeDto, Token));

            // Assert
            Assert.NotNull(exception);
        }

        [Fact]
        public async Task EditMethod_Throws_Exception_When_PreOrder_IsAuthorized_And_Incorrect_UserId()
        {
            // Arrange
            var currentUserId = "currentUserId";
            var preorderUserId = Guid.NewGuid().ToString();
            var preOrder = await EntityHelper.CreatePreOrderAsync(_mainUnitOfWork, preorderUserId);
            var changeDto = new PreOrderChangeDto
            {
                UniqId = preOrder.Id,
                ChangedUserId = currentUserId
            };
            // Act
            var exception =
                await Assert.ThrowsAsync<BusinessLogicException>(async () => await _manager.ChangeAsync(changeDto, Token));
            // Assert
            Assert.NotNull(exception);
            Assert.Equal(
                Resources.Exceptions.PreOrderManager_Change_Текущий_пользователь_не_может_получить_доступ_к_данному_заказу,
                exception.Message);
        }

        [Fact]
        public async Task EditMethod_Shoud_Update_EntityAsync()
        {
            // Arrange
            var userId = Guid.NewGuid().ToString();
            var preOrder = await EntityHelper.CreatePreOrderAsync(_mainUnitOfWork, userId);

            var locationFromLng = 3;
            var locationFromLat = 3;
            var locationFromAddress = "3";
            var locationToLng = 4;
            var locationToLat = 4;
            var locationToAddress = "4";

            // Act
            var id = await _manager.ChangeAsync(new PreOrderChangeDto
            {
                LocationFrom = new LocationDto
                {
                    Lat = locationFromLat,
                    Lng = locationFromLng,
                    Address = locationFromAddress
                },
                LocationTo = new LocationDto
                {
                    Lat = locationToLat,
                    Lng = locationToLng,
                    Address = locationToAddress
                },
                IsAuthorized = true,
                ChangedUserId = userId,
                UniqId = preOrder.Id
            }, Token);

            // Assert
            var preOrderFromDb = _mainUnitOfWork.PreOrders.Get(preOrder.Id);
            Assert.False(preOrderFromDb.Id == Guid.Empty);
            Assert.Equal(preOrder.Id, preOrderFromDb.Id);
            Assert.Equal(preOrder.Id, id);
            Assert.True(preOrderFromDb.IsAuthorized);
            Assert.False(preOrderFromDb.IsDeleted);
            Assert.False(preOrderFromDb.IsOrderCreated);

            Assert.NotNull(preOrderFromDb.LocationFrom);
            Assert.Equal(locationFromLat, preOrderFromDb.LocationFrom.Lat);
            Assert.Equal(locationFromLng, preOrderFromDb.LocationFrom.Lng);
            Assert.Equal(locationFromAddress, preOrderFromDb.LocationFrom.Address);

            Assert.NotNull(preOrderFromDb.LocationTo);
            Assert.Equal(locationToLat, preOrderFromDb.LocationTo.Lat);
            Assert.Equal(locationToLng, preOrderFromDb.LocationTo.Lng);
            Assert.Equal(locationToAddress, preOrderFromDb.LocationTo.Address);

            Assert.NotNull(preOrderFromDb.Modified);
            Assert.Equal(userId, preOrderFromDb.CreatedUserId);
        }
    }
}