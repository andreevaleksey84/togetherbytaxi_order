﻿using System;
using System.Threading.Tasks;
using Order.BusinessLogic.Exceptions;
using Order.BusinessLogic.Models;
using Xunit;

namespace Order.BusinessLogic.Managers.Tests.PreOrderManager
{
    public partial class PreOrderManagerTests
    {
        [Fact]
        public async Task GetByUniqIdMethod_Null_Dto_Should_Throw_BusinessLogicArgumentNullException()
        {
            // Arrange

            // Act
            var exception = await Assert.ThrowsAsync<BusinessLogicArgumentNullException>(async () => await _manager.GetByUniqIdAsync(null, Token));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal($"Model {nameof(PreOrderGetDto)} is empty", exception.Message);
        }

        [Fact]
        public async Task GetByUniqIdMethod_Throws_Exception_When_UniqId_Not_Found()
        {
            // Arrange
            var preOrderGetDto = new PreOrderGetDto { UniqId = Guid.NewGuid() };

            // Act
            var exception =
                await Assert.ThrowsAsync<EntityNotFoundException>(async () => await _manager.GetByUniqIdAsync(preOrderGetDto, Token));

            // Assert
            Assert.NotNull(exception);
        }

        [Fact]
        public async Task GetByUniqIdMethod_Throws_Exception_When_PreOrder_IsAuthorized_And_Incorrect_UserId()
        {
            // Arrange
            var currentUserId = "currentUserId";
            var preorderUserId = Guid.NewGuid().ToString();
            var preOrder = await EntityHelper.CreatePreOrderAsync(_mainUnitOfWork, preorderUserId);
            var preOrderGetDto = new PreOrderGetDto
            {
                UniqId = preOrder.Id,
                CurrentUserId = currentUserId
            };
            // Act
            var exception =
                await Assert.ThrowsAsync<BusinessLogicException>(async () => await _manager.GetByUniqIdAsync(preOrderGetDto, Token));
            // Assert
            Assert.NotNull(exception);
            Assert.Equal(
                Resources.Exceptions.PreOrderManager_Change_Текущий_пользователь_не_может_получить_доступ_к_данному_заказу,
                exception.Message);
        }
    }
}
