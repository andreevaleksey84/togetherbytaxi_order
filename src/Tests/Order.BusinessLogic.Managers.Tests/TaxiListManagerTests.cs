﻿using Order.BusinessLogic.Managers.Abstraction;
using System.Threading.Tasks;
using Xunit;

namespace Order.BusinessLogic.Managers.Tests
{
    public partial class TaxiListManagerTests
    {
        private readonly ITaxiListManager _manager;

        public TaxiListManagerTests()
        {
            _manager = new TaxiListManager();
        }

        [Fact]
        public async Task GetAllAsync_CorrectResult()
        {
            // Arrange

            // Act
            var results = await _manager.GetAllAsync();

            // Assert
            Assert.NotNull(results);
            Assert.Equal(2, results.Count);
        }
    }
}
