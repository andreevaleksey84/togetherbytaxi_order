﻿using System.Threading.Tasks;
using System;
using Order.DataAccess.Repositories.Abstraction;
using Order.DataAccess.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Order.BusinessLogic.Managers.Tests
{
    public static class EntityHelper
    {
        public static async Task<DataAccess.Models.Order> CreateOrderAsync(IMainUnitOfWork mainUnitOfWork, string createdUserId)
        {
            var orderUniqId = Guid.NewGuid();
            var order = new DataAccess.Models.Order
            {
                Id = orderUniqId,
                Created = DateTime.UtcNow,
                IsDeleted = false,
                Modified = null,
            };
            var preOrder = await CreatePreOrderAsync(mainUnitOfWork, createdUserId, saveChanges: false);
            order.PreOrders = new List<PreOrder>
            {
                preOrder
            };
            mainUnitOfWork.Orders.Add(order);
            await mainUnitOfWork.SaveChangesAsync(CancellationToken.None);
            return order;
        }

        public static async Task<PreOrder> CreatePreOrderAsync(IMainUnitOfWork mainUnitOfWork, string createdUserId, bool saveChanges = true)
        {
            var preOrderUniqId = Guid.NewGuid();
            var preOrder = new PreOrder
            {
                Id = preOrderUniqId,
                CreatedUserId = createdUserId,
                IsAuthorized = true,
                Created = DateTime.UtcNow,
                IsDeleted = false,
                IsOrderCreated = false,
                LocationFrom = new Location
                {
                    Lat = 1,
                    Lng = 1,
                    Address = "1"
                },
                LocationTo = new Location
                {
                    Lat = 2,
                    Lng = 2,
                    Address = "2"
                },
                Modified = null,
            };
            mainUnitOfWork.PreOrders.Add(preOrder);
            if (saveChanges)
            {
                await mainUnitOfWork.SaveChangesAsync(CancellationToken.None);
            }
            return preOrder;
        }

        public static async Task DeleteAllOrderAndPreOrderForUserAsync(IMainUnitOfWork mainUnitOfWork, string createdUserId)
        {
            var orders = mainUnitOfWork.Orders.GetByConditionAsync(x => x.PreOrders.Any(p => p.CreatedUserId == createdUserId));
            await foreach (var order in orders)
            {
                mainUnitOfWork.Orders.Delete(order);
            }
            await mainUnitOfWork.SaveChangesAsync(CancellationToken.None);

            var preorders = mainUnitOfWork.PreOrders.GetByConditionAsync(x => x.CreatedUserId == createdUserId);
            await foreach (var preorder in preorders)
            {
                mainUnitOfWork.PreOrders.Delete(preorder);
            }
            await mainUnitOfWork.SaveChangesAsync(CancellationToken.None);
        }
    }
}
