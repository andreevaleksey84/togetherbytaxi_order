﻿using System;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using Moq;
using Order.BusinessLogic.Exceptions;
using Order.BusinessLogic.Models;
using Order.DataAccess.Models;
using Order.DataAccess.Repositories.Abstraction;
using Xunit;

namespace Order.BusinessLogic.Managers.Tests.OrderManager
{
    public partial class OrderManagerTests
    {
        [Fact]
        public async Task CreateMethod_Null_Dto_Should_Throw_BusinessLogicArgumentNullException()
        {
            // Arrange

            // Act
            var exception = await Assert.ThrowsAsync<BusinessLogicArgumentNullException>(async () => await _manager.CreateAsync(null, Token));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal($"Model {nameof(OrderCreateDto)} is empty", exception.Message);
        }

        [Fact]
        public async Task CreateMethod_Empty_Dto_Should_Throw_BusinessLogicInvalidArgumentException()
        {
            // Arrange

            // Act
            var exception = await Assert.ThrowsAsync<BusinessLogicInvalidArgumentException>(async () => await _manager.CreateAsync(new OrderCreateDto(), Token));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal("Field PreOrderUniqId is invalid. Must be set", exception.Message);
        }

        [Fact]
        public async Task CreateMethod_NotExisting_PreOrder_Should_Throw_EntityNotFoundException()
        {
            // Arrange
            var preOrderId = Guid.NewGuid();
            var dto = new OrderCreateDto
            { 
                PreOrderUniqId = preOrderId,
            };

            // Act
            var exception = await Assert.ThrowsAsync<EntityNotFoundException>(async () => await _manager.CreateAsync(dto, Token));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal($"Entity PreOrder not found({preOrderId})", exception.Message);
        }

        [Fact]
        public async Task CreateMethod_PreOrder_AlreadyWithOrder_Should_Throw_BusinessLogicException()
        {
            // Arrange
            var createdUserId = Guid.NewGuid().ToString();
            var preOrder = await EntityHelper.CreatePreOrderAsync(_mainUnitOfWork, createdUserId);
            var order = await EntityHelper.CreateOrderAsync(_mainUnitOfWork, createdUserId);
            preOrder.OrderId = order.Id;
            await _mainUnitOfWork.SaveChangesAsync(Token);
            var dto = new OrderCreateDto
            {
                PreOrderUniqId = preOrder.Id,
            };

            // Act
            var exception = await Assert.ThrowsAsync<BusinessLogicException>(async () => await _manager.CreateAsync(dto, Token));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal($"Order already set for PreOrder({preOrder.Id})", exception.Message);
        }

        [Fact]
        public async Task CreateMethod_PreOrder_When_IsOrderCreated_True_Should_Throw_BusinessLogicException()
        {
            // Arrange
            var createdUserId = Guid.NewGuid().ToString();
            var preOrder = await EntityHelper.CreatePreOrderAsync(_mainUnitOfWork, createdUserId);
            await EntityHelper.CreateOrderAsync(_mainUnitOfWork, createdUserId);
            preOrder.IsOrderCreated = true;
            await _mainUnitOfWork.SaveChangesAsync(Token);
            var dto = new OrderCreateDto
            {
                PreOrderUniqId = preOrder.Id,
            };

            // Act
            var exception = await Assert.ThrowsAsync<BusinessLogicException>(async () => await _manager.CreateAsync(dto, Token));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal($"Order already created for PreOrder({preOrder.Id})", exception.Message);
        }

        [Fact]
        public async Task CreateMethod_PreOrder_When_CreatedUserId_Null_Should_Throw_BusinessLogicException()
        {
            // Arrange
            var createdUserId = string.Empty;
            var preOrder = await EntityHelper.CreatePreOrderAsync(_mainUnitOfWork, createdUserId);
            await _mainUnitOfWork.SaveChangesAsync(Token);
            var dto = new OrderCreateDto
            {
                PreOrderUniqId = preOrder.Id,
            };

            // Act
            var exception = await Assert.ThrowsAsync<BusinessLogicException>(async () => await _manager.CreateAsync(dto, Token));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal($"PreOrder created with not authorized user", exception.Message);
        }

        [Fact]
        public async Task CreateMethod_PreOrder_When_IsAuthorized_False_Should_Throw_BusinessLogicException()
        {
            // Arrange
            var createdUserId = Guid.NewGuid().ToString();
            var preOrder = await EntityHelper.CreatePreOrderAsync(_mainUnitOfWork, createdUserId);
            preOrder.IsAuthorized = false;
            await _mainUnitOfWork.SaveChangesAsync(Token);
            var dto = new OrderCreateDto
            {
                PreOrderUniqId = preOrder.Id,
            };

            // Act
            var exception = await Assert.ThrowsAsync<BusinessLogicException>(async () => await _manager.CreateAsync(dto, Token));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal($"PreOrder created with not authorized user", exception.Message);
        }

        [Fact]
        public async Task CreateMethod_PreOrder_When_IncorrectUser_Should_Throw_BusinessLogicException()
        {
            // Arrange
            var preOrderUserId = Guid.NewGuid().ToString();
            var currentUserId = Guid.NewGuid().ToString();
            var preOrder = await EntityHelper.CreatePreOrderAsync(_mainUnitOfWork, preOrderUserId);
            var dto = new OrderCreateDto
            {
                PreOrderUniqId = preOrder.Id,
                CurrentUserId = currentUserId,
            };

            // Act
            var exception = await Assert.ThrowsAsync<BusinessLogicException>(async () => await _manager.CreateAsync(dto, Token));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal($"PreOrder does not belong to current user id", exception.Message);
        }

        [Fact]
        public async Task CreateMethod_CorrectResult()
        {
            // Arrange
            var userId = Guid.NewGuid().ToString();
            var preOrder = await EntityHelper.CreatePreOrderAsync(_mainUnitOfWork, userId);
            var dto = new OrderCreateDto
            {
                PreOrderUniqId = preOrder.Id,
                CurrentUserId = userId,
            };

            // Act
            var orderCreateResult = await _manager.CreateAsync(dto, Token);

            // Assert
            Assert.NotNull(orderCreateResult);
            Assert.NotEqual(Guid.Empty, orderCreateResult.Id);
            var preOrderFromDb = await _mainUnitOfWork.PreOrders.GetAsync(preOrder.Id, Token);
            Assert.NotNull(preOrderFromDb);
            Assert.True(preOrderFromDb.IsOrderCreated);
        }

        [Fact]
        public async Task CreateMethod_CorrectResult_WithMockAsync()
        {
            // Arrange
            var userId = _autoFixture.Create<string>();

            var mainUnitOfWorkMock = new Mock<IMainUnitOfWork>();
            var orderRepository = new Mock<IOrderRepository>();
            var preOrder1Id = _autoFixture.Create<Guid>();
            mainUnitOfWorkMock.Setup(x => x.Orders).Returns(orderRepository.Object);
            var preOrderRepository = new Mock<IPreOrderRepository>();
            var preOrder = new PreOrder
            {
                Id = preOrder1Id,
                OrderId = null,
                Order = null,
                CreatedUserId = userId,
                IsAuthorized = true,
            };
            preOrderRepository
                .Setup(x => x.GetAsync(preOrder1Id, Token))
                .Returns(new ValueTask<PreOrder>(preOrder));
            mainUnitOfWorkMock.Setup(x => x.PreOrders).Returns(preOrderRepository.Object);
            var mapperMock = new Mock<IMapper>();
            var order1Id = _autoFixture.Create<Guid>();
            mapperMock
                .Setup(x => x.Map<OrderCreateDto, DataAccess.Models.Order>(It.Is<OrderCreateDto>(o => o.PreOrderUniqId == preOrder1Id)))
                .Returns(new DataAccess.Models.Order
                {
                    Id = order1Id
                });
            mapperMock
                .Setup(x => x.Map<DataAccess.Models.Order, OrderCreateResultDto>(It.Is<DataAccess.Models.Order>(o => o.Id == order1Id)))
                .Returns(new OrderCreateResultDto
                {
                    Id = order1Id
                });
            _manager = new Managers.OrderManager(mainUnitOfWorkMock.Object, mapperMock.Object, _orderManagerLoggerMock.Object);

            var dto = new OrderCreateDto
            {
                PreOrderUniqId = preOrder1Id,
                CurrentUserId = userId,
            };

            // Act
            var orderCreateResult = await _manager.CreateAsync(dto, Token);

            // Assert
            Assert.NotNull(orderCreateResult);
            Assert.Equal(order1Id, orderCreateResult.Id);
            // mainUnitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Exactly(2));
        }
    }
}
