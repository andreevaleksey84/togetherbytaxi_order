using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using Order.BusinessLogic.Managers.Abstraction;
using Order.DataAccess.Repositories.Abstraction;
using Xunit;

namespace Order.BusinessLogic.Managers.Tests.OrderManager
{
    public partial class OrderManagerTests : BaseUnitTests, IClassFixture<Fixture>
    {
        private IOrderManager _manager;
        private readonly IMainUnitOfWork _mainUnitOfWork;
        private readonly Mock<ILogger<Managers.OrderManager>> _orderManagerLoggerMock = new Mock<ILogger<Managers.OrderManager>>();
        private readonly IMapper _mapper;
        private readonly AutoFixture.Fixture _autoFixture;

        public OrderManagerTests(Fixture fixture)
        {
            _mapper = fixture.ServiceProvider.GetService<IMapper>();
            _mainUnitOfWork =  fixture.ServiceProvider.GetService<IMainUnitOfWork>();

            _manager = new Managers.OrderManager(_mainUnitOfWork, _mapper, _orderManagerLoggerMock.Object);
            _autoFixture = new AutoFixture.Fixture();
        }
    }
}