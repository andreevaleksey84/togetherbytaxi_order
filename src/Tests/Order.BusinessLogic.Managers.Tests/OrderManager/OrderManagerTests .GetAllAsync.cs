﻿namespace Order.BusinessLogic.Managers.Tests.OrderManager
{
    /*
    public partial class OrderManagerTests
    {
        [Fact]
        public async Task GetAllAsync_Null_Dto_Should_Throw_BusinessLogicArgumentNullException()
        {
            // Arrange

            // Act
            var exception = await Assert.ThrowsAsync<BusinessLogicArgumentNullException>(async () => await _manager.GetAllAsync(null));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal($"Model {nameof(OrderGetAllDto)} is empty", exception.Message);
        }

        [Fact]
        public async Task GetAllAsync_NotExisting_CurrentUserId_Must_Return_EmptyArrayAsync()
        {
            // Arrange
            var currentUserId = _autoFixture.Create<string>();
            var dto = new OrderGetAllDto
            {
                CurrentUserId = currentUserId
            };

            // Act
            var result = await _manager.GetAllAsync(dto);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(0, result.Count);
        }

        [Fact]
        public async Task GetAllAsync_NotExisting_CurrentUserId_With_DeletedOrders_Must_Return_EmptyArrayAsync()
        {
            // Arrange
            var currentUserId = _autoFixture.Create<string>();
            var dto = new OrderGetAllDto
            {
                CurrentUserId = currentUserId
            };

            await EntityHelper.CreatePreOrderAsync(_mainUnitOfWork, currentUserId);
            await EntityHelper.CreateOrderAsync(_mainUnitOfWork, currentUserId);

            await EntityHelper.DeleteAllOrderAndPreOrderForUserAsync(_mainUnitOfWork, currentUserId);

            // Act
            var result = await _manager.GetAllAsync(dto);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(0, result.Count);
        }


        [Fact]
        public async Task GetAllAsync_CurrentUserId_Must_Return_DataAsync()
        {
            // Arrange
            var currentUserId = _autoFixture.Create<string>();
            var dto = new OrderGetAllDto
            {
                CurrentUserId = currentUserId
            };

            await EntityHelper.CreatePreOrderAsync(_mainUnitOfWork, currentUserId);
            await EntityHelper.CreateOrderAsync(_mainUnitOfWork, currentUserId);

            // Act
            var result = await _manager.GetAllAsync(dto);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(1, result.Count);
        }

        [Fact]
        public async Task GetAllAsync_CurrentUserId_Must_Return_Data_WithMockAsync()
        {
            // Arrange
            var currentUserId = _autoFixture.Create<string>();
            var mainUnitOfWorkMock = new Mock<IMainUnitOfWork>();
            var orderRepository = new Mock<IOrderRepository>();
            var order1Id = _autoFixture.Create<Guid>();
            var preOrder1Id = _autoFixture.Create<Guid>();
            var preOrder1Created = _autoFixture.Create<DateTime>();
            var preOrder1AddressFrom = _autoFixture.Create<string>();
            var preOrder1LatFrom = _autoFixture.Create<double>();
            var preOrder1LngFrom = _autoFixture.Create<double>();
            var preOrder1AddressTo = _autoFixture.Create<string>();
            var preOrder1LatTo = _autoFixture.Create<double>();
            var preOrder1LngTo = _autoFixture.Create<double>();
            var orders = new List<DataAccess.Models.Order>
            {
                new DataAccess.Models.Order
                {
                    Id = order1Id,
                    IsDeleted = false,
                    Created = preOrder1Created,
                    Modified = DateTime.UtcNow,
                    PreOrders = new List<DataAccess.Models.PreOrder>
                    {
                        new DataAccess.Models.PreOrder
                        {
                            Id = preOrder1Id,
                            IsDeleted = false,
                            IsAuthorized = true,
                            Created = DateTime.UtcNow,
                            CreatedUserId = currentUserId,
                            IsOrderCreated = true,
                            OrderId = order1Id,
                            Modified = DateTime.UtcNow,
                            ModifiedUserId = currentUserId,
                            LocationFrom = new DataAccess.Models.Location
                            {
                                Address = preOrder1AddressFrom,
                                Lat = preOrder1LatFrom,
                                Lng = preOrder1LngFrom
                            },
                            LocationTo = new DataAccess.Models.Location
                            {
                                Address = preOrder1AddressTo,
                                Lat = preOrder1LatTo,
                                Lng = preOrder1LngTo
                            }
                        }
                    }
                }
            };
            orderRepository
                .Setup(x => x.GetAllForUserAsync(currentUserId))
                .Returns(orders.ToAsyncEnumerable());
            mainUnitOfWorkMock.Setup(x => x.Orders).Returns(orderRepository.Object);
            _manager = new Managers.OrderManager(mainUnitOfWorkMock.Object, _mapper, _orderManagerLoggerMock.Object);

            var dto = new OrderGetAllDto
            {
                CurrentUserId = currentUserId
            };

            // Act
            var result = await _manager.GetAllAsync(dto);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(1, result.Count);
            var firstResult = result.First();
            Assert.NotNull(firstResult);
            Assert.Equal(1, firstResult.Number);
            Assert.Equal(preOrder1Created, firstResult.Date);
            Assert.Equal(order1Id, firstResult.OrderId);
            Assert.NotNull(firstResult.From);
            Assert.Equal(preOrder1AddressFrom, firstResult.From.Address);
            Assert.Equal(preOrder1LatFrom, firstResult.From.Lat);
            Assert.Equal(preOrder1LngFrom, firstResult.From.Lng);
            Assert.NotNull(firstResult.To);
            Assert.Equal(preOrder1AddressTo, firstResult.To.Address);
            Assert.Equal(preOrder1LatTo, firstResult.To.Lat);
            Assert.Equal(preOrder1LngTo, firstResult.To.Lng);
        }
    }
    */
}
