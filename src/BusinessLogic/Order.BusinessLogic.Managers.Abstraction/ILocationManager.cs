﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Order.BusinessLogic.Models;

namespace Order.BusinessLogic.Managers.Abstraction
{
    /// <summary>
    /// Менеджер для работы с местоположением
    /// </summary>
    public interface ILocationManager
    {
        /// <summary>
        /// Найти ближайшее метоположение
        /// </summary>
        /// <param name="findNearbyLocationDto"><see cref="FindNearbyLocationDto"/></param>
        /// <returns>Массив <see cref="FoundLocationDto"/></returns>
        Task<ICollection<FoundLocationDto>> FindNearbyAsync(FindNearbyLocationDto findNearbyLocationDto);
    }
}
