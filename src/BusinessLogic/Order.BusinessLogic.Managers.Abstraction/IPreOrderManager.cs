﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Order.BusinessLogic.Models;

namespace Order.BusinessLogic.Managers.Abstraction
{
    /// <summary>
    /// PreOrder manager
    /// </summary>
    public interface IPreOrderManager
    {
        /// <summary>
        /// Create preorder
        /// </summary>
        /// <param name="createDto"><see cref="PreOrderCreateDto"/></param>
        /// <returns><see cref="PreOrderCreateResultDto"/></returns>
        Task<PreOrderCreateResultDto> CreateAsync(PreOrderCreateDto createDto, CancellationToken cancellationToken);

        /// <summary>
        /// Change preorder
        /// </summary>
        /// <param name="createDto"><see cref="PreOrderChangeDto"/></param>
        /// <returns></returns>
        Task<Guid> ChangeAsync(PreOrderChangeDto createDto, CancellationToken cancellationToken);

        /// <summary>
        /// Get preorder by id
        /// </summary>
        /// <param name="getDto"><see cref="PreOrderGetDto"/></param>
        /// <returns><see cref="PreOrderGetResultDto"/></returns>
        Task<PreOrderGetResultDto> GetByUniqIdAsync(PreOrderGetDto getDto, CancellationToken cancellationToken);
    }
}