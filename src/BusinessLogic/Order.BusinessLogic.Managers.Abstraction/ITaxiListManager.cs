﻿using Order.BusinessLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Order.BusinessLogic.Managers.Abstraction
{
    /// <summary>
    /// Менеджер для Списка такси
    /// </summary>
    public interface ITaxiListManager
    {
        /// <summary>
        /// Получить информацию по всем такси
        /// </summary>
        /// <returns>Массив <see cref="TaxiListDto"/></returns>
        Task<ICollection<TaxiListDto>> GetAllAsync();
    }
}
