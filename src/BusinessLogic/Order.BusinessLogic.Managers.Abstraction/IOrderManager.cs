﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Order.BusinessLogic.Models;
using Order.BusinessLogic.Models.Orders;

namespace Order.BusinessLogic.Managers.Abstraction
{
    /// <summary>
    /// Order manager
    /// </summary>
    public interface IOrderManager
    {
        /// <summary>
        /// Create order
        /// </summary>
        /// <param name="createDto"><see cref="OrderCreateDto"/></param>
        /// <param name="cancellationToken"><see cref="CancellationToken"/></param>
        /// <returns><see cref="OrderCreateResultDto"/></returns>
        Task<OrderCreateResultDto> CreateAsync(OrderCreateDto createDto, CancellationToken cancellationToken);

        /// <summary>
        /// Get all orders
        /// </summary>
        /// <param name="dto"><see cref="OrderGetAllDto"/></param>
        /// <param name="cancellationToken"><see cref="CancellationToken"/></param>
        /// <returns><see cref="ICollection{OrderInfoDto}"/></returns>
        Task<ICollection<OrderInfoDto>> GetAllAsync(OrderGetAllDto dto, CancellationToken cancellationToken);

        /// <summary>
        /// Delete order
        /// </summary>
        /// <param name="deleteDto"><see cref="OrderDeleteDto"/></param>
        /// <param name="cancellationToken"><see cref="CancellationToken"/></param>
        /// <returns><see cref="Task"/></returns>
        Task DeleteAsync(OrderDeleteDto deleteDto, CancellationToken cancellationToken);
    }
}
