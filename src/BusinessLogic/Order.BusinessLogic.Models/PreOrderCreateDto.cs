﻿namespace Order.BusinessLogic.Models
{
    public class PreOrderCreateDto
    {
        public string CreatedUserId { get; set; }
        public bool IsAuthorized { get; set; }
        public LocationDto LocationFrom { get; set; }
        public LocationDto LocationTo { get; set; }
    }
}
