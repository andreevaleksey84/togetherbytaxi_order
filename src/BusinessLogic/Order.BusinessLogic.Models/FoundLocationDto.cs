﻿using System;

namespace Order.BusinessLogic.Models
{
    public class FoundLocationDto
    {
        /// <summary>
        /// Точно то же место, откуда идёт поиск
        /// </summary>
        public bool SameLocation { get; set; }

        /// <summary>
        /// Дистанция
        /// </summary>
        public int Distance { get; set; }

        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Является ли пользователь авторизованным
        /// </summary>
        public bool IsAuthorizedUser { get; set; }

        /// <summary>
        /// Наденное место откуда едет
        /// </summary>
        public LocationDto FromLocation { get; set; }

        /// <summary>
        /// Наденное место куда едет
        /// </summary>
        public LocationDto ToLocation { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Как давно
        /// </summary>
        public string HowLong { get; set; }
    }
}
