﻿namespace Order.BusinessLogic.Models
{
    /// <summary>
    /// FindNearbyLocationDto
    /// </summary>
    public class FindNearbyLocationDto
    {
        public LocationWithGetCoordinateDto LocationFromSearch { get; set; }

        /// <summary>
        /// SearchInLocation
        /// </summary>
        public SearchLocationEnum SearchInLocation { get; set; }

        /// <summary>
        /// UserId
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Distance to search
        /// </summary>
        public double Distance { get; set; }
    }
}
