﻿using GeoCoordinatePortable;

namespace Order.BusinessLogic.Models
{
    public class LocationWithGetCoordinateDto
    {
        public GeoCoordinate GeoCoordinate { get; set; }
        public string Address { get; set; }
    }
}
