﻿namespace Order.BusinessLogic.Models
{
    public class LocationDto
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string Address { get; set; }
    }
}
