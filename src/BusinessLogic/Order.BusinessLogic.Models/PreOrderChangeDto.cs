﻿using System;

namespace Order.BusinessLogic.Models
{
    public class PreOrderChangeDto
    {
        public Guid UniqId { get; set; }
        public string ChangedUserId { get; set; }
        public bool IsAuthorized { get; set; }
        public LocationDto LocationFrom { get; set; }
        public LocationDto LocationTo { get; set; }
    }
}
