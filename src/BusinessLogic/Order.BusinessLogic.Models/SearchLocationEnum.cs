﻿namespace Order.BusinessLogic.Models
{
    public enum SearchLocationEnum
    {
        Unknown = 0,
        LocationFrom = 5,
        LocationTo = 10,
        LocationFromAndTo = 15
    }
}
