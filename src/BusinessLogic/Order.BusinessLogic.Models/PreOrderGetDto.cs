﻿using System;

namespace Order.BusinessLogic.Models
{
    public class PreOrderGetDto
    {
        public Guid UniqId { get; set; }
        public string CurrentUserId { get; set; }
        public bool IsAuthorized { get; set; }
    }
}