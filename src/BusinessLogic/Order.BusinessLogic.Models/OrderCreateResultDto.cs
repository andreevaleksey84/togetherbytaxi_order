﻿using System;

namespace Order.BusinessLogic.Models
{
    public class OrderCreateResultDto
    {
        public Guid Id { get; set; }
    }
}
