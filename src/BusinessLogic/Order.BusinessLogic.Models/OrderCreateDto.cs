﻿using System;

namespace Order.BusinessLogic.Models
{
    /// <summary>
    /// Model to create order
    /// </summary>
    public class OrderCreateDto
    {
        /// <summary>
        /// PreOrderUniqId
        /// </summary>
        public Guid PreOrderUniqId { get; set; }

        /// <summary>
        /// Current user id
        /// </summary>
        public string CurrentUserId { get; set; }
    }
}
