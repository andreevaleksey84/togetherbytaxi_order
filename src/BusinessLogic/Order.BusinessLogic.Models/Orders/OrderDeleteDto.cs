﻿using System;

namespace Order.BusinessLogic.Models.Orders;

/// <summary>
/// Model to delete order
/// </summary>
public class OrderDeleteDto
{
    /// <summary>
    /// Order Id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Current user id
    /// </summary>
    public string CurrentUserId { get; set; }
}
