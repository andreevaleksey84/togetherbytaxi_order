﻿using System;

namespace Order.BusinessLogic.Models.Orders
{
    public class OrderInfoDto
    {
        /// <summary>
        /// Идентификатор заказа
        /// </summary>
        public Guid OrderId { get; set; }

        /// <summary>
        /// Номер заказа
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Откуда
        /// </summary>
        public LocationDto From { get; set; }

        /// <summary>
        /// Куда
        /// </summary>
        public LocationDto To { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public DateTime Date { get; set; }
    }
}
