﻿namespace Order.BusinessLogic.Models.Orders
{
    /// <summary>
    /// Model to get all orders for user
    /// </summary>
    public class OrderGetAllDto
    {
        /// <summary>
        /// Current user id
        /// </summary>
        public string CurrentUserId { get; set; }
    }
}
