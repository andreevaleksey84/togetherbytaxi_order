﻿using System;

namespace Order.BusinessLogic.Models
{
    public class PreOrderCreateResultDto
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public string CreatedUserId { get; set; }
        public bool IsAuthorized { get; set; }
        public LocationDto LocationFrom { get; set; }
        public LocationDto LocationTo { get; set; }
        public bool IsOrderCreated { get; set; }
        public bool IsDeleted { get; set; }
    }
}
