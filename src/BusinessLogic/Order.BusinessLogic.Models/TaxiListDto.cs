﻿using System;

namespace Order.BusinessLogic.Models
{
    public class TaxiListDto
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string[] Phones { get; set; }
        public string Site { get; set; }
    }
}
