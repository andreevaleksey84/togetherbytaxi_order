﻿using System;

namespace Order.BusinessLogic.Exceptions
{
    public class BusinessLogicInvalidArgumentException : Exception
    {
        public BusinessLogicInvalidArgumentException()
        {
        }

        public BusinessLogicInvalidArgumentException(string entityName)
            : base($"Field {entityName} is invalid")
        {
        }

        public BusinessLogicInvalidArgumentException(string entityName, string validationException)
            : base($"Field {entityName} is invalid. {validationException}")
        {
        }
    }
}
