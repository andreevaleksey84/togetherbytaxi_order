﻿using System;

namespace Order.BusinessLogic.Exceptions
{
    public class BusinessLogicArgumentNullException : Exception
    {
        public BusinessLogicArgumentNullException()
        {
        }

        public BusinessLogicArgumentNullException(string entityName)
            : base($"Model {entityName} is empty")
        {
        }
    }
}