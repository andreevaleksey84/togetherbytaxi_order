﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using Order.BusinessLogic.Exceptions;
using Order.BusinessLogic.Managers.Abstraction;
using Order.BusinessLogic.Models;
using Order.BusinessLogic.Models.Orders;
using Order.DataAccess.Models;
using Order.DataAccess.Repositories.Abstraction;

namespace Order.BusinessLogic.Managers;

/// <inheritdoc/>
public class OrderManager : IOrderManager
{
    private readonly IMainUnitOfWork _mainUnitOfWork;
    private readonly IMapper _mapper;
    private readonly ILogger<OrderManager> _logger;
    public OrderManager(
        IMainUnitOfWork mainUnitOfWork,
        IMapper mapper,
        ILogger<OrderManager> logger
        )
    {
        _mainUnitOfWork = mainUnitOfWork;
        _mapper = mapper;
        _logger = logger;
    }

    /// <inheritdoc/>
    public async Task<OrderCreateResultDto> CreateAsync(OrderCreateDto createDto, CancellationToken cancellationToken)
    {
        _logger.LogDebug("We in create order method. {createDto}", createDto);
        ValidateCreateOrder(createDto);
        var preOrder = await _mainUnitOfWork.PreOrders.GetAsync(createDto.PreOrderUniqId, cancellationToken);
        ValidatePreOrderFromDb();

        var order = _mapper.Map<OrderCreateDto, DataAccess.Models.Order>(createDto);
        _mainUnitOfWork.Orders.Add(order);
        await _mainUnitOfWork.SaveChangesAsync(cancellationToken);

        preOrder.Order = order;
        preOrder.OrderId = order.Id;
        preOrder.IsOrderCreated = true;

        await _mainUnitOfWork.SaveChangesAsync(cancellationToken);

        _logger.LogDebug("Order created. {order}", order);
        var orderCreateResultDto = _mapper.Map<DataAccess.Models.Order, OrderCreateResultDto>(order);
        return orderCreateResultDto;

        void ValidateCreateOrder(OrderCreateDto createDto)
        {
            if (createDto == null)
            {
                throw new BusinessLogicArgumentNullException(nameof(OrderCreateDto));
            }

            if (createDto.PreOrderUniqId == Guid.Empty)
            {
                throw new BusinessLogicInvalidArgumentException(nameof(OrderCreateDto.PreOrderUniqId), "Must be set");
            }
        }

        void ValidatePreOrderFromDb()
        {
            if (preOrder == null)
            {
                throw new EntityNotFoundException(nameof(PreOrder), createDto.PreOrderUniqId.ToString());
            }
            if (preOrder.OrderId.HasValue)
            {
                throw new BusinessLogicException($"Order already set for PreOrder({createDto.PreOrderUniqId})");
            }
            if (preOrder.IsOrderCreated)
            {
                throw new BusinessLogicException($"Order already created for PreOrder({createDto.PreOrderUniqId})");
            }
            if (string.IsNullOrEmpty(preOrder.CreatedUserId))
            {
                throw new BusinessLogicException("PreOrder created with not authorized user");
            }
            if (!preOrder.IsAuthorized)
            {
                throw new BusinessLogicException("PreOrder created with not authorized user");
            }
            if (preOrder.CreatedUserId != createDto.CurrentUserId)
            {
                throw new BusinessLogicException("PreOrder does not belong to current user id");
            }
        }
    }

    /// <inheritdoc/>
    public async Task<ICollection<OrderInfoDto>> GetAllAsync(OrderGetAllDto dto, CancellationToken cancellationToken)
    {
        CheckDto();

        var orders = _mainUnitOfWork.Orders.GetAllForUserAsync(dto.CurrentUserId);
        var results = new List<OrderInfoDto>();
        var index = 1;
        await foreach (var order in orders)
        {
            var result = _mapper.Map<DataAccess.Models.Order, OrderInfoDto>(order);
            result.Number = index;
            index++;
            results.Add(result);
        }

        return results;
        void CheckDto()
        {
            if (dto == null)
            {
                throw new BusinessLogicArgumentNullException(nameof(OrderGetAllDto));
            }
        }
    }

    /// <inheritdoc/>
    public async Task DeleteAsync(OrderDeleteDto deleteDto, CancellationToken cancellationToken)
    {
        _logger.LogDebug("We in delete order method. {createDto}", deleteDto);
        ValidateDeleteOrder(deleteDto);
        var order = await _mainUnitOfWork.Orders.GetByIdForUserAsync(deleteDto.Id, deleteDto.CurrentUserId, cancellationToken);
        ValidateOrderFromDb();

        order.IsDeleted = true;
        foreach (var preOrder in order.PreOrders)
        {
            preOrder.IsDeleted = true;
        }
        await _mainUnitOfWork.SaveChangesAsync(cancellationToken);

        _logger.LogDebug("Order deleted");

        void ValidateDeleteOrder(OrderDeleteDto deleteDto)
        {
            if (deleteDto == null)
            {
                throw new BusinessLogicArgumentNullException(nameof(OrderDeleteDto));
            }

            if (deleteDto.Id == Guid.Empty)
            {
                throw new BusinessLogicInvalidArgumentException(nameof(OrderDeleteDto.Id), "Must be set");
            }
        }

        void ValidateOrderFromDb()
        {
            if (order == null)
            {
                throw new EntityNotFoundException(nameof(Order), deleteDto.Id.ToString());
            }
            
           
            if (order.IsDeleted)
            {
                throw new BusinessLogicException("Order already deleted");
            }

            if (order.PreOrders == null)
            {
                throw new BusinessLogicException("PreOrder does not exists");
            }

            if (order.PreOrders.Any(x => x.CreatedUserId != deleteDto.CurrentUserId))
            {
                throw new BusinessLogicException("Order does not belong to current user id");
            }
        }
    }
}
