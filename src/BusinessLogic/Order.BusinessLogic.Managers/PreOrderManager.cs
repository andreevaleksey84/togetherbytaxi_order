﻿using System;
using Order.BusinessLogic.Managers.Abstraction;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using Order.BusinessLogic.Models;
using Order.DataAccess.Models;
using Order.DataAccess.Repositories.Abstraction;
using Order.BusinessLogic.Exceptions;
using System.Threading;

namespace Order.BusinessLogic.Managers
{
    /// <summary>
    /// PreOrder manager
    /// </summary>
    public class PreOrderManager : IPreOrderManager
    {
        private readonly IMainUnitOfWork _mainUnitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger<PreOrderManager> _logger;

        public PreOrderManager(IMainUnitOfWork mainUnitOfWork,
            IMapper mapper,
            ILogger<PreOrderManager> logger)
        {
            _mainUnitOfWork = mainUnitOfWork;
            _mapper = mapper;
            _logger = logger;
        }

        /// <inheritdoc/>
        public async Task<PreOrderCreateResultDto> CreateAsync(PreOrderCreateDto createDto, CancellationToken cancellationToken)
        {
            _logger.LogDebug("We in create preorder method. {createDto}", createDto);
            ValidateCreatePreOrder(createDto);
            var preOrder = _mapper.Map<PreOrderCreateDto, PreOrder>(createDto);
            _mainUnitOfWork.PreOrders.Add(preOrder);
            await _mainUnitOfWork.SaveChangesAsync(cancellationToken);
            _logger.LogDebug("PreOrder created. {preOrder}", preOrder);
            var preOrderDtoResult = _mapper.Map<PreOrder, PreOrderCreateResultDto>(preOrder);
            return preOrderDtoResult;

            void ValidateCreatePreOrder(PreOrderCreateDto createDto)
            {
                if (createDto == null)
                {
                    throw new BusinessLogicArgumentNullException(nameof(PreOrderCreateDto));
                }
            }
        }

        /// <inheritdoc/>
        public async Task<Guid> ChangeAsync(PreOrderChangeDto changeDto, CancellationToken cancellationToken)
        {
            _logger.LogDebug("We in change preorder method. {changeDto}", changeDto);
            ValidateChangePreOrder(changeDto);
            var currentPreOrder = await _mainUnitOfWork.PreOrders.GetAsync(changeDto.UniqId, cancellationToken);
            if (currentPreOrder == null)
            {
                throw new EntityNotFoundException(nameof(PreOrder), changeDto.UniqId.ToString());
            }

            if (currentPreOrder.IsAuthorized && currentPreOrder.CreatedUserId != changeDto.ChangedUserId)
            {
                throw new BusinessLogicException(Resources.Exceptions
                    .PreOrderManager_Change_Текущий_пользователь_не_может_получить_доступ_к_данному_заказу);
            }

            SetAllFieldsForUpdate(currentPreOrder, changeDto);
            await _mainUnitOfWork.SaveChangesAsync(cancellationToken);
            _logger.LogDebug("PreOrder changed. {preOrder}", currentPreOrder);
            return currentPreOrder.Id;

            void ValidateChangePreOrder(PreOrderChangeDto changeDto)
            {
                if (changeDto == null)
                {
                    throw new BusinessLogicArgumentNullException(nameof(PreOrderChangeDto));
                }
            }

            void SetAllFieldsForUpdate(PreOrder preOrder, PreOrderChangeDto preOrderChangeDto)
            {
                preOrder.Modified = DateTime.UtcNow;
                preOrder.ModifiedUserId = changeDto.ChangedUserId;
                preOrder.LocationFrom = _mapper.Map<LocationDto, Location>(preOrderChangeDto.LocationFrom);
                preOrder.LocationTo = _mapper.Map<LocationDto, Location>(preOrderChangeDto.LocationTo);
            }
        }

        /// <inheritdoc/>
        public async Task<PreOrderGetResultDto> GetByUniqIdAsync(PreOrderGetDto orderGetDto, CancellationToken cancellationToken)
        {
            _logger.LogDebug("We in GetByUniqId preorder method. {orderGetDto}", orderGetDto);
            CheckForNull();

            var currentPreOrder = await _mainUnitOfWork.PreOrders.GetAsync(orderGetDto.UniqId, cancellationToken);
            if (currentPreOrder == null)
            {
                throw new EntityNotFoundException(nameof(PreOrder), orderGetDto.UniqId.ToString());
            }

            if (currentPreOrder.IsAuthorized && currentPreOrder.CreatedUserId != orderGetDto.CurrentUserId)
            {
                throw new BusinessLogicException(Resources.Exceptions
                    .PreOrderManager_Change_Текущий_пользователь_не_может_получить_доступ_к_данному_заказу);
            }

            _logger.LogDebug("PreOrder result. {preOrder}", currentPreOrder);
            var preOrderDtoResult = _mapper.Map<PreOrder, PreOrderGetResultDto>(currentPreOrder);
            return preOrderDtoResult;

            void CheckForNull()
            {
                if (orderGetDto == null)
                {
                    throw new BusinessLogicArgumentNullException(nameof(PreOrderGetDto));
                }
            }
        }
    }
}