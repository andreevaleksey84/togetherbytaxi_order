﻿using Order.BusinessLogic.Managers.Abstraction;
using Order.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Order.BusinessLogic.Managers
{
    public class TaxiListManager : ITaxiListManager
    {
        public Task<ICollection<TaxiListDto>> GetAllAsync() =>
            Task.FromResult((ICollection<TaxiListDto>)new List<TaxiListDto>
            {
                new TaxiListDto
                {
                    Id = Guid.NewGuid(),
                    Code = "Maxim",
                    Name = "Maxim",
                    Description = "Севастопольское такси Максим",
                    Phones = new List<string> { "+7 (978) 707-77-77", "+7 (978) 999-00-00" }.ToArray(),
                    Site = "https://spravkataxi.ru/sevastopol/maxim"
                },
                new TaxiListDto
                {
                    Id = Guid.NewGuid(),
                    Code = "FirstTaxi",
                    Name = "Первое такси",
                    Description = "Первое такси Севастополь, Симферополь, Джанкой, Бахчисарай, Балаклава",
                    Phones = new List<string> { "+7 (978) 333-33-33" }.ToArray(),
                    Site = "https://pervoe-taxi.ru/"
                }
            });
    }
}
