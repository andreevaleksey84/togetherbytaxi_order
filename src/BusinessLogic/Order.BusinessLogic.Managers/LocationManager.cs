﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeoCoordinatePortable;
using Humanizer;
using Microsoft.Extensions.Logging;
using Order.BusinessLogic.Exceptions;
using Order.BusinessLogic.Managers.Abstraction;
using Order.BusinessLogic.Models;
using Order.DataAccess.Repositories.Abstraction;

namespace Order.BusinessLogic.Managers
{
    /// <summary>
    /// Location Manager
    /// </summary>
    public class LocationManager : ILocationManager
    {
        private readonly IPreOrderRepository _preOrderRepository;
        private readonly ILogger<LocationManager> _logger;

        public LocationManager(
            IPreOrderRepository preOrderRepository,
            ILogger<LocationManager> logger
            )
        {
            _preOrderRepository = preOrderRepository;
            _logger = logger;
        }

        /// <inheritdoc/>
        public async Task<ICollection<FoundLocationDto>> FindNearbyAsync(FindNearbyLocationDto findNearbyLocationDto)
        {
            _logger.LogDebug("We in Find Nearby location method. {findNearbyLocationDto}", findNearbyLocationDto);
            await ValidateDto(findNearbyLocationDto);
            var startCoordinate = findNearbyLocationDto.LocationFromSearch.GeoCoordinate;
            var preOrders = await _preOrderRepository
                .GetAllByDateMoreThan(DateTime.UtcNow.AddDays(-7), findNearbyLocationDto.UserId);

            if (preOrders == null || !preOrders.Any())
            {
                return new List<FoundLocationDto>();
            }

            switch (findNearbyLocationDto.SearchInLocation)
            {
                case SearchLocationEnum.LocationFrom:
                    return preOrders
                        .Where(x => CheckLocation(x.LocationFrom))
                        .Select(x => CreateLocation(x, x.LocationFrom))
                        .ToList();
                case SearchLocationEnum.LocationTo:
                    return preOrders
                        .Where(x => CheckLocation(x.LocationTo))
                        .Select(x => CreateLocation(x, x.LocationTo))
                        .ToList();
                case SearchLocationEnum.LocationFromAndTo:
                    var fromLocations = preOrders
                        .Where(x => CheckLocation(x.LocationFrom))
                        .Select(x => CreateLocation(x, x.LocationFrom))
                        .ToList();
                    var toLocations = preOrders
                        .Where(x => CheckLocation(x.LocationTo))
                        .Select(x => CreateLocation(x, x.LocationTo))
                        .ToList();
                    fromLocations.AddRange(toLocations);
                    return fromLocations;

                default:
                    throw new BusinessLogicException("Incorrect search parameters");
            }

            FoundLocationDto CreateLocation(DataAccess.Models.PreOrder preOrder, DataAccess.Models.Location location)
            {
                var distance = Convert.ToInt32(Math.Round(GetDistanceToStartCoordinate(location), 0, MidpointRounding.AwayFromZero));
                return new FoundLocationDto
                {
                    IsAuthorizedUser = preOrder.IsAuthorized,
                    UserId = preOrder.CreatedUserId,
                    CreatedDate = preOrder.Created,
                    FromLocation = new LocationDto
                    {
                        Lat = preOrder.LocationFrom.Lat,
                        Lng = preOrder.LocationFrom.Lng,
                        Address = preOrder.LocationFrom.Address,
                    },
                    ToLocation = new LocationDto
                    {
                        Lat = preOrder.LocationTo.Lat,
                        Lng = preOrder.LocationTo.Lng,
                        Address = preOrder.LocationTo.Address,
                    },
                    SameLocation = distance == 0,
                    Distance = distance,
                    HowLong = HowLongCalculation(preOrder.Created)
                };
            }

            double GetDistanceToStartCoordinate(DataAccess.Models.Location locationToCheck) =>
                startCoordinate.GetDistanceTo(new GeoCoordinate(locationToCheck.Lat, locationToCheck.Lng));

            bool CheckLocation(DataAccess.Models.Location locationToCheck) =>
                GetDistanceToStartCoordinate(locationToCheck) <= findNearbyLocationDto.Distance;

            string HowLongCalculation(DateTime createdDate)
            {
                var diff = DateTime.UtcNow - createdDate;
                return diff.Humanize();
            }
        }

        private Task ValidateDto(FindNearbyLocationDto findNearbyLocationDto)
        {
            if (findNearbyLocationDto == null)
            {
                throw new BusinessLogicArgumentNullException(nameof(FindNearbyLocationDto));
            }

            if (findNearbyLocationDto.LocationFromSearch == null)
            {
                throw new ArgumentNullException(nameof(findNearbyLocationDto.LocationFromSearch));
            }

            if (findNearbyLocationDto.LocationFromSearch.GeoCoordinate == null)
            {
                throw new ArgumentNullException(nameof(findNearbyLocationDto.LocationFromSearch.GeoCoordinate));
            }

            if (findNearbyLocationDto.SearchInLocation == SearchLocationEnum.Unknown)
            {
                throw new Exception("Please set search location");
            }

            if (string.IsNullOrEmpty(findNearbyLocationDto.UserId))
            {
                throw new Exception("UserId is not defined");
            }

            return Task.FromResult(true);
        }
    }
}
