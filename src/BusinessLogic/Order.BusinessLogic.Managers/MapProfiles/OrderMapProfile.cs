﻿using System;
using System.Linq;
using AutoMapper;
using Order.BusinessLogic.Models;
using Order.BusinessLogic.Models.Orders;

namespace Order.BusinessLogic.Managers.MapProfiles
{
    /// <summary>
    /// Профайл для маппинга
    /// </summary>
    public class OrderMapProfile : Profile
    {
        public OrderMapProfile()
        {
            CreateMap<OrderCreateDto, DataAccess.Models.Order>()
                .ForMember(d => d.Id, map => map.MapFrom(s => Guid.NewGuid()))
                .ForMember(d => d.Modified, map => map.Ignore())
                .ForMember(d => d.IsDeleted, map => map.MapFrom(s => false))
                .ForMember(d => d.Created, map => map.MapFrom(s => DateTime.UtcNow))
                .ForMember(d => d.PreOrders, map => map.Ignore());

            CreateMap<DataAccess.Models.Order, OrderCreateResultDto>()
                .ForMember(d => d.Id, map => map.MapFrom(s => s.Id));

            CreateMap<DataAccess.Models.Order, OrderInfoDto>()
                .ForMember(d => d.OrderId, map => map.MapFrom(s => s.Id))
                .ForMember(d => d.From, map => map.MapFrom(s => s.PreOrders.FirstOrDefault().LocationFrom))
                .ForMember(d => d.To, map => map.MapFrom(s => s.PreOrders.FirstOrDefault().LocationTo))
                .ForMember(d => d.Date, map => map.MapFrom(s => s.Created))
                .ForMember(d => d.Number, map => map.Ignore());
        }
    }
}
