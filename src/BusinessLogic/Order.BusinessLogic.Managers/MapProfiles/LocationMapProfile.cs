﻿using AutoMapper;
using Order.BusinessLogic.Models;
using Order.DataAccess.Models;

namespace Order.BusinessLogic.Managers.MapProfiles
{
    /// <summary>
    /// Профайл для маппинга
    /// </summary>
    public class LocationMapProfile : Profile
    {
        public LocationMapProfile()
        {
            CreateMap<LocationDto, Location>();
            CreateMap<Location, LocationDto>();
        }
    }
}
