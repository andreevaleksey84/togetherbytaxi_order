﻿using System;
using AutoMapper;
using Order.BusinessLogic.Models;
using Order.DataAccess.Models;

namespace Order.BusinessLogic.Managers.MapProfiles
{
    /// <summary>
    /// Профайл для маппинга
    /// </summary>
    public class PreOrderMapProfile : Profile
    {
        public PreOrderMapProfile()
        {
            CreateMap<PreOrderCreateDto, PreOrder>()
                .ForMember(d => d.Id, map => map.MapFrom(s => Guid.NewGuid()))
                .ForMember(d => d.ModifiedUserId, map => map.Ignore())
                .ForMember(d => d.Modified, map => map.Ignore())
                .ForMember(d => d.IsDeleted, map => map.MapFrom(s => false))
                .ForMember(d => d.IsOrderCreated, map => map.MapFrom(s => false))
                .ForMember(d => d.Created, map => map.MapFrom(s => DateTime.UtcNow))
                .ForMember(d => d.OrderId, map => map.Ignore())
                .ForMember(d => d.Order, map => map.Ignore());

            CreateMap<PreOrder, PreOrderCreateResultDto>();
            CreateMap<PreOrder, PreOrderGetResultDto>()
                .ForMember(d => d.UniqIdentifier, map => map.MapFrom(s => s.Id));
        }
    }
}
