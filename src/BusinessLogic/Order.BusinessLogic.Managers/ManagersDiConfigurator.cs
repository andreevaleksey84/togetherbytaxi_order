﻿using Order.BusinessLogic.Managers.Abstraction;
using Order.BusinessLogic.Managers;
using AutoMapper;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ManagersDiConfigurator
    {
        /// <summary>
        /// ConfigureManagers
        /// </summary>
        public static IServiceCollection ConfigureManagers(this IServiceCollection services) => services
            .AddTransient<IPreOrderManager, PreOrderManager>()
            .AddTransient<IOrderManager, OrderManager>()
            .AddTransient<ILocationManager, LocationManager>()
            .AddTransient<ITaxiListManager, TaxiListManager>();

        public static void AddBlMapperConfiguration(this IMapperConfigurationExpression cfg)
        {
            cfg.AddProfile<Order.BusinessLogic.Managers.MapProfiles.PreOrderMapProfile>();
            cfg.AddProfile<Order.BusinessLogic.Managers.MapProfiles.LocationMapProfile>();
            cfg.AddProfile<Order.BusinessLogic.Managers.MapProfiles.OrderMapProfile>();
        }
    }
}
