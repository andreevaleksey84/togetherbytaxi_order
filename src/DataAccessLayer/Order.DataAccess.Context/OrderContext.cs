﻿using Microsoft.EntityFrameworkCore;
using Order.DataAccess.Models;

namespace Order.DataAccess.Context
{
    /// <summary>
    /// OrderContext
    /// </summary>
    public class OrderContext : DbContext
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="options"></param>
        public OrderContext(DbContextOptions<OrderContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PreOrder>().OwnsOne(typeof(Location), "LocationFrom");
            modelBuilder.Entity<PreOrder>().OwnsOne(typeof(Location), "LocationTo");
            base.OnModelCreating(modelBuilder);
        }

        /// <summary>
        /// Pre orders
        /// </summary>
        public DbSet<PreOrder> PreOrders { get; set; }

        /// <summary>
        /// Orders
        /// </summary>
        public DbSet<Models.Order> Orders { get; set; }
    }
}
