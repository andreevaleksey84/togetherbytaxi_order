﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Order.DataAccess.Context
{
    /// <summary>
    /// Фабрика создания миграций
    /// </summary>
    public class OrderContextFactory : IDesignTimeDbContextFactory<OrderContext>
    {
        public OrderContext CreateDbContext(string[] args)
        {
            var dbContextOptionsBuilder = new DbContextOptionsBuilder<OrderContext>();
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            var configuration = builder.Build();

            var connectionString = GetSettingFromConfig(configuration, "ConnectionStrings", "TogetherByTaxiConnection");

            if (string.IsNullOrEmpty(connectionString))
            {
                throw new Exception("Configuration setting does not exist.Setting name ConnectionStrings:TogetherByTaxiConnection");
            }
            dbContextOptionsBuilder.UseNpgsql(connectionString, opt => opt.MigrationsAssembly("Order.DataAccess.Context"));
            Console.WriteLine($"connectionString - {connectionString}");
            return new OrderContext(dbContextOptionsBuilder.Options);
        }

        public static string GetSettingFromConfig(IConfiguration configuration, string firstName,
            string secondName, bool throwIfEmpty = true)
        {
            return GetSettingFromConfig((IConfigurationRoot) configuration, firstName, secondName, throwIfEmpty);
        }

        public static string GetSettingFromConfig(IConfigurationRoot configurationRoot, string firstName, string secondName, bool throwIfEmpty = true)
        {
            var result = configurationRoot[$"{firstName}:{secondName}"];
            if (string.IsNullOrEmpty(result))
            {
                result = configurationRoot[$"{firstName}_{secondName}"];
            }

            if (string.IsNullOrEmpty(result) && throwIfEmpty)
            {
                throw new Exception($"Configuration setting does not exist. Setting name {firstName}:{secondName}");
            }

            return result;
        }
    }
}
