﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Order.DataAccess.Repositories.Abstraction
{
    public interface IOrderRepository : IRepositoryBase<Models.Order, Guid>
    {
        IAsyncEnumerable<Models.Order> GetAllForUserAsync(string userId);

        Task<Models.Order> GetByIdForUserAsync(Guid id, string userId, CancellationToken cancellationToken);
    }
}
