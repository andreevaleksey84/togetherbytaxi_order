﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Order.DataAccess.Repositories.Abstraction
{
    public interface IRepositoryBase
    {
    }

    public interface IRepositoryBase<T, in TPrimaryKey> : IRepositoryBase
    {
        /// <summary>
        /// Запросить все сущности в базе
        /// </summary>
        /// <param name="asNoTracking">Вызвать с AsNoTracking</param>
        /// <returns>IQueryable массив сущностей</returns>
        IEnumerable<T> GetAll(bool asNoTracking = false);

        /// <summary>
        /// Получить все сущности
        /// </summary>
        /// <returns></returns>
        IAsyncEnumerable<T> GetAllAsync();

        /// <summary>
        /// Получить все сущности по условию
        /// </summary>
        /// <param name="expression">дерево выражений, лямбда</param>
        /// <returns>IAsyncEnumerable<typeparamref name="T"/></returns>
        IAsyncEnumerable<T> GetByConditionAsync(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns>сущность</returns>
        T Get(TPrimaryKey id);

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns>сущность</returns>
        ValueTask<T> GetAsync(TPrimaryKey id, CancellationToken cancellationToken);

        /// <summary>
        /// Удалить сущность
        /// </summary>
        /// <param name="id">ID удалённой сущности</param>
        /// <returns>была ли сущность удалена</returns>
        bool Delete(TPrimaryKey id);

        /// <summary>
        /// Удалить сущность
        /// </summary>
        /// <param name="entity">сущность для удаления</param>
        /// <returns>была ли сущность удалена</returns>
        bool Delete(T entity);

        /// <summary>
        /// Удалить сущности
        /// </summary>
        /// <param name="entities">Коллекция сущностей для удаления</param>
        /// <returns>была ли операция завершена успешно</returns>
        bool DeleteRange(ICollection<T> entities);

        /// <summary>
        /// Для сущности проставить состояние - что она изменена
        /// </summary>
        /// <param name="entity">сущность для изменения</param>
        void Update(T entity);

        /// <summary>
        /// Для сущности проставить состояние - что она изменена
        /// </summary>
        /// <param name="entity">сущность для изменения</param>
        Task UpdateAsync(T item);

        /// <summary>
        /// Добавить в базу одну сущность
        /// </summary>
        /// <param name="entity">сущность для добавления</param>
        /// <returns>добавленная сущность</returns>
        T Add(T entity);

        /// <summary>
        /// Добавить в базу массив сущностей
        /// </summary>
        /// <param name="entities">массив сущностей</param>
        void AddRange(IEnumerable<T> entities);

        /// <summary>
        /// Добавить в базу одну сущность
        /// </summary>
        /// <param name="entity">сущность для добавления</param>
        /// <returns>добавленная сущность</returns>
        Task<T> AddAsync(T item);

        /// <summary>
        /// Добавить в базу массив сущностей
        /// </summary>
        /// <param name="entities">массив сущностей</param>
        Task AddRangeAsync(IEnumerable<T> items);
    }
}
