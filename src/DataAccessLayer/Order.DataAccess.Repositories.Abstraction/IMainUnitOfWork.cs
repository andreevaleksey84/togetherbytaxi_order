﻿using System.Threading;
using System.Threading.Tasks;

namespace Order.DataAccess.Repositories.Abstraction
{
    public interface IMainUnitOfWork
    {
        IOrderRepository Orders { get; }

        IPreOrderRepository PreOrders { get; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        int SaveChanges();
    }
}
