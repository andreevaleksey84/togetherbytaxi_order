﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Order.DataAccess.Models;

namespace Order.DataAccess.Repositories.Abstraction
{
    public interface IPreOrderRepository : IRepositoryBase<PreOrder, Guid>
    {
        Task<ICollection<PreOrder>> GetAllByUniqIdAsync(ICollection<Guid> unitIds);
        Task<ICollection<PreOrder>> GetAllByDateMoreThan(DateTime dateTimeFrom, string currentUserId);
    }
}
