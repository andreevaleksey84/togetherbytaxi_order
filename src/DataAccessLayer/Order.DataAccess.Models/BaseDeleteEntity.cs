﻿namespace Order.DataAccess.Models
{
    public class BaseDeleteEntity<T> : BaseEntity<T>
    {
        public bool IsDeleted { get; set; }
    }
}
