﻿using System;

namespace Order.DataAccess.Models
{
    public class PreOrder : BaseDeleteEntity<Guid>
    {
        public string CreatedUserId { get; set; }
        public string ModifiedUserId { get; set; }
        public bool IsAuthorized { get; set; }
        public Location LocationFrom { get; set; }
        public Location LocationTo { get; set; }
        public bool IsOrderCreated { get; set; }

        public Guid? OrderId { get; set; }
        public Order Order { get; set; }
    }
}
