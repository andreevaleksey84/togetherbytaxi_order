﻿using System;
using System.Collections.Generic;

namespace Order.DataAccess.Models
{
    public class Order : BaseDeleteEntity<Guid>
    {
        public ICollection<PreOrder> PreOrders { get; set; }
    }
}
