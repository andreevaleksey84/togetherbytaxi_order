﻿using Order.DataAccess.Context;
using Order.DataAccess.Repositories.Abstraction;
using System.Threading;
using System.Threading.Tasks;

namespace Order.DataAccess.Repositories
{
    public class MainUnitOfWork : IMainUnitOfWork
    {
        private readonly OrderContext _dbCtx;
        private IOrderRepository _orderRepository;
        private IPreOrderRepository _preOrderRepository;

        public MainUnitOfWork(OrderContext dbCtx) => _dbCtx = dbCtx;

        public IOrderRepository Orders => _orderRepository ??= new OrderRepository(_dbCtx);

        public IPreOrderRepository PreOrders => _preOrderRepository ??= new PreOrderRepository(_dbCtx);

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken) => await _dbCtx.SaveChangesAsync(cancellationToken);

        public int SaveChanges() => _dbCtx.SaveChanges();
    }
}
