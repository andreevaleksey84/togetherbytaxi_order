﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Order.DataAccess.Repositories.Abstraction;

namespace Order.DataAccess.Repositories
{
    public class OrderRepository : RepositoryBase<Models.Order, Guid>, IOrderRepository
    {
        public OrderRepository(DbContext context) : base(context)
        {
        }

        public IAsyncEnumerable<Models.Order> GetAllForUserAsync(string userId) =>
            EntitySet
            .Include(x => x.PreOrders)
            .Where(x => !x.IsDeleted && x.PreOrders.Any(x => x.CreatedUserId == userId))
            .AsAsyncEnumerable();

        public Task<Models.Order> GetByIdForUserAsync(Guid id, string userId, CancellationToken cancellationToken) =>
            EntitySet
            .Include(x => x.PreOrders)
            .Where(x => x.Id == id && !x.IsDeleted && x.PreOrders.All(po => po.CreatedUserId == userId && !po.IsDeleted))
            .SingleOrDefaultAsync(cancellationToken);
    }
}
