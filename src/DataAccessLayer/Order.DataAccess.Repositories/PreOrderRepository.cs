﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Order.DataAccess.Models;
using Order.DataAccess.Repositories.Abstraction;

namespace Order.DataAccess.Repositories
{
    public class PreOrderRepository : RepositoryBase<PreOrder, Guid>, IPreOrderRepository
    {
        public PreOrderRepository(DbContext context) : base(context)
        {
        }

        public async Task<ICollection<PreOrder>> GetAllByDateMoreThan(DateTime dateTimeFrom, string currentUserId) =>
            await EntitySet
                .Where(x =>
                    x.IsAuthorized &&
                    x.CreatedUserId != currentUserId &&
                    !x.IsDeleted && !x.IsOrderCreated && (x.Created > dateTimeFrom && x.Modified == null) ||
                    (x.Modified != null && x.Modified > dateTimeFrom))
                .ToArrayAsync();

        public async Task<ICollection<PreOrder>> GetAllByUniqIdAsync(ICollection<Guid> unitIds) =>
            await EntitySet
                .Where(x => x.IsDeleted == false && unitIds.Contains(x.Id))
                .ToArrayAsync();
    }
}
