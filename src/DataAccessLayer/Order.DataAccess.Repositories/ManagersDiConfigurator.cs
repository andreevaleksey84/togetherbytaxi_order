﻿using Order.DataAccess.Repositories.Abstraction;
using Order.DataAccess.Repositories;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ManagersDiConfigurator
    {
        /// <summary>
        /// ConfigureRepositories
        /// </summary>
        public static IServiceCollection ConfigureRepositories(this IServiceCollection services) => services
            .AddTransient<IPreOrderRepository, PreOrderRepository>()
            .AddScoped<IMainUnitOfWork, MainUnitOfWork>()
            .AddTransient<IOrderRepository, OrderRepository>();
    }
}