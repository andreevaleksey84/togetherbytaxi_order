﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Order.DataAccess.Repositories.Abstraction;

namespace Order.DataAccess.Repositories
{
    public class RepositoryBase<T, TPrimaryKey> : IRepositoryBase<T, TPrimaryKey> where T : class
    {
        protected readonly DbContext Context;
        protected DbSet<T> EntitySet;

        protected RepositoryBase(DbContext context)
        {
            Context = context;
            EntitySet = Context.Set<T>();
        }

        /// <inheritdoc/>
        public virtual IEnumerable<T> GetAll(bool asNoTracking = false)
        {
            return asNoTracking ? EntitySet.AsNoTracking().ToList() : EntitySet.ToList();
        }

        /// <inheritdoc/>
        public virtual IAsyncEnumerable<T> GetAllAsync() => EntitySet.AsAsyncEnumerable();

        /// <inheritdoc/>
        public IAsyncEnumerable<T> GetByConditionAsync(Expression<Func<T, bool>> expression) => EntitySet.Where(expression).AsAsyncEnumerable();

        /// <inheritdoc/>
        public virtual T Get(TPrimaryKey id) =>
            EntitySet.Find(id);

        /// <inheritdoc/>
        public virtual async ValueTask<T> GetAsync(TPrimaryKey id, CancellationToken cancellationToken) =>
            await EntitySet.FindAsync([id], cancellationToken: cancellationToken);

        /// <inheritdoc/>
        public virtual bool Delete(TPrimaryKey id)
        {
            var obj = EntitySet.Find(id);
            if (obj == null)
            {
                return false;
            }
            EntitySet.Remove(obj);
            return true;
        }

        /// <inheritdoc/>
        public virtual bool Delete(T entity)
        {
            if (entity == null)
            {
                return false;
            }
            Context.Entry(entity).State = EntityState.Deleted;
            return true;
        }

        /// <inheritdoc/>
        public virtual bool DeleteRange(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return false;
            }
            EntitySet.RemoveRange(entities);
            return true;
        }

        /// <inheritdoc/>
        public virtual void Update(T entity) =>
            Context.Entry(entity).State = EntityState.Modified;

        /// <inheritdoc/>
        public Task UpdateAsync(T item) => 
            EntitySet.SingleUpdateAsync(item);

        /// <inheritdoc/>
        public virtual T Add(T entity) =>
            Context.Set<T>().Add(entity).Entity;

        /// <inheritdoc/>
        public virtual void AddRange(IEnumerable<T> entities)
        {
            var enumerable = entities as IList<T> ?? entities.ToList();
            Context.Set<T>().AddRange(enumerable);
        }

        /// <inheritdoc/>
        public async Task<T> AddAsync(T item) => 
            (await EntitySet.AddAsync(item)).Entity;

        /// <inheritdoc/>
        public async Task AddRangeAsync(IEnumerable<T> items) => 
            await EntitySet.AddRangeAsync(items);
    }
}
