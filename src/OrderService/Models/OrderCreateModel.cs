﻿using System;
using System.Collections.Generic;

namespace OrderService.Models
{
    /// <summary>
    /// OrderCreateModel
    /// </summary>
    public class OrderCreateModel
    {
        /// <summary>
        /// PreOrderUniqId
        /// </summary>
        public Guid PreOrderUniqId { get; set; }

        /// <summary>
        /// LinkedPreOrderUniqIds
        /// </summary>
        public List<Guid> LinkedPreOrderUniqIds { get; set; }
    }
}
