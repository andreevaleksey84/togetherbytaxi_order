﻿namespace OrderService.Models.Location.Responses
{
    /// <summary>
    /// Местоположение
    /// </summary>
    public class LocationResponse
    {
        /// <summary>
        /// Latitude
        /// </summary>
        public double Lat { get; set; }

        /// <summary>
        /// Longitude
        /// </summary>
        public double Lng { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string Address { get; set; }
    }
}
