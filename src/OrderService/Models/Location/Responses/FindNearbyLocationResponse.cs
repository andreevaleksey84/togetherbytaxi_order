﻿using OrderService.Models.Location.Requests;
using System;

namespace OrderService.Models.Location.Responses
{
    /// <summary>
    /// Ответ на запрос <see cref="FindNearbyLocationModelRequest"/>
    /// </summary>
    public class FindNearbyLocationResponse
    {
        /// <summary>
        /// Точно то же место, откуда идёт поиск
        /// </summary>
        public bool SameLocation { get; set; }

        /// <summary>
        /// Точно то же место, откуда идёт поиск
        /// </summary>
        public int Distance { get; set; }

        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Является ли пользователь заказа авторизованным
        /// </summary>
        public bool IsAuthorizedUser { get; set; }

        /// <summary>
        /// Местоположение откуда
        /// </summary>
        public LocationResponse FromLocation { get; set; }

        /// <summary>
        /// Местоположение куда
        /// </summary>
        public LocationResponse ToLocation { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Как давно
        /// </summary>
        public string HowLong { get; set; }
    }
}
