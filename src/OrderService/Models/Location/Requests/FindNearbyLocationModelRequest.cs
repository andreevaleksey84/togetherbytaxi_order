﻿namespace OrderService.Models.Location.Requests
{
    /// <summary>
    /// FindNearbyLocationModel
    /// </summary>
    public class FindNearbyLocationModelRequest
    {
        /// <summary>
        /// Latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Longitude
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Distance
        /// </summary>
        public double Distance { get; set; }

        /// <summary>
        /// SearchInLocationFrom
        /// </summary>
        public bool SearchInLocationFrom { get; set; }

        /// <summary>
        /// SearchInLocationFrom
        /// </summary>
        public bool SearchInLocationTo { get; set; }
    }
}
