﻿using System;
using AutoMapper;
using Order.BusinessLogic.Models;
using OrderService.Models;

namespace OrderService.MapProfiles
{
    /// <summary>
    /// Профайл для маппинга
    /// </summary>
    public class PreOrderMapProfile : Profile
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public PreOrderMapProfile()
        {
            CreateMap<PreOrderCreateModel, PreOrderCreateDto>()
                .ForMember(d => d.CreatedUserId, map => map.Ignore())
                .ForMember(d => d.IsAuthorized, map => map.Ignore())
                .ForMember(d => d.LocationFrom, map => map.MapFrom(s => MapLocationFrom(s)))
                .ForMember(d => d.LocationTo, map => map.MapFrom(s => MapLocationTo(s)));

            CreateMap<PreOrderChangeModel, PreOrderChangeDto>()
                .ForMember(d => d.UniqId, map => map.Ignore())
                .ForMember(d => d.ChangedUserId, map => map.Ignore())
                .ForMember(d => d.IsAuthorized, map => map.Ignore())
                .ForMember(d => d.LocationFrom, map => map.MapFrom(s => MapLocationFrom(s)))
                .ForMember(d => d.LocationTo, map => map.MapFrom(s => MapLocationTo(s)));

            CreateMap<Guid, PreOrderGetDto>()
                .ForMember(d => d.UniqId, map => map.MapFrom(s => s))
                .ForMember(d => d.IsAuthorized, map => map.Ignore())
                .ForMember(d => d.CurrentUserId, map => map.Ignore());
        }

        private LocationDto MapLocationFrom(PreOrderCreateModel createModel)
        {
            if (createModel == null)
            {
                return new LocationDto();
            }

            return new LocationDto
            {
                Lat = createModel.LocationFromLat,
                Lng = createModel.LocationFromLng,
                Address = createModel.LocationFromAddress
            };
        }

        private LocationDto MapLocationTo(PreOrderCreateModel createModel)
        {
            if (createModel == null)
            {
                return new LocationDto();
            }

            return new LocationDto
            {
                Lat = createModel.LocationToLat,
                Lng = createModel.LocationToLng,
                Address = createModel.LocationToAddress
            };
        }

        private LocationDto MapLocationFrom(PreOrderChangeModel createModel)
        {
            if (createModel == null)
            {
                return new LocationDto();
            }

            return new LocationDto
            {
                Lat = createModel.LocationFromLat,
                Lng = createModel.LocationFromLng,
                Address = createModel.LocationFromAddress
            };
        }

        private LocationDto MapLocationTo(PreOrderChangeModel createModel)
        {
            if (createModel == null)
            {
                return new LocationDto();
            }

            return new LocationDto
            {
                Lat = createModel.LocationToLat,
                Lng = createModel.LocationToLng,
                Address = createModel.LocationToAddress
            };
        }
    }
}
