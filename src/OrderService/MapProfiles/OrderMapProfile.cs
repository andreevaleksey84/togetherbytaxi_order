﻿using AutoMapper;
using Order.BusinessLogic.Models;
using OrderService.Models;

namespace OrderService.MapProfiles
{
    /// <summary>
    /// Профайл для маппинга
    /// </summary>
    public class OrderMapProfile : Profile
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public OrderMapProfile()
        {
            CreateMap<OrderCreateModel, OrderCreateDto>()
                .ForMember(d => d.CurrentUserId, map => map.Ignore());
        }
    }
}
