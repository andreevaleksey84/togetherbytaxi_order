﻿using AutoMapper;
using Order.BusinessLogic.Models;
using OrderService.Models.Location.Requests;

namespace OrderService.MapProfiles
{
    /// <summary>
    /// Профайл для маппинга
    /// </summary>
    public class LocationMapProfile : Profile
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public LocationMapProfile()
        {
            CreateMap<FindNearbyLocationModelRequest, FindNearbyLocationDto>()
                .ForMember(d => d.LocationFromSearch, map => map.MapFrom(s => MapLocationFromSearch(s)))
                .ForMember(d => d.Distance, map => map.MapFrom(s => s.Distance))
                .ForMember(d => d.SearchInLocation, map => map.MapFrom(s => MapSearchInLocation(s)))
                .ForMember(d => d.UserId, map => map.Ignore());
        }

        private LocationWithGetCoordinateDto MapLocationFromSearch(FindNearbyLocationModelRequest model)
        {
            if (model == null)
            {
                return new LocationWithGetCoordinateDto();
            }

            return new LocationWithGetCoordinateDto
            {
                GeoCoordinate = new GeoCoordinatePortable.GeoCoordinate(model.Latitude, model.Longitude)
            };
        }

        private SearchLocationEnum MapSearchInLocation(FindNearbyLocationModelRequest model)
        {
            if (model == null)
            {
                return SearchLocationEnum.Unknown;
            }

            if (model.SearchInLocationFrom && model.SearchInLocationTo)
            {
                return SearchLocationEnum.LocationFromAndTo;
            }

            if (model.SearchInLocationFrom)
            {
                return SearchLocationEnum.LocationFrom;
            }

            if (model.SearchInLocationTo)
            {
                return SearchLocationEnum.LocationTo;
            }

            return SearchLocationEnum.Unknown;
        }
    }
}
