﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using Order.BusinessLogic.Exceptions;

namespace OrderService.Middlewares
{
    /// <summary>
    /// Логирование необработанных контроллерами исключений
    /// </summary>
    public class ErrorHandlingMiddleware
    {
        private static readonly JsonSerializerSettings _settings = new()
        {
            ContractResolver = new DefaultContractResolver
            {
                NamingStrategy = new CamelCaseNamingStrategy
                {
                    OverrideSpecifiedNames = false
                }
            }
        };

        private readonly RequestDelegate _next;

        /// <summary>
        /// Конструктор для создания c DI
        /// </summary>
        /// <param name="next"></param>
        public ErrorHandlingMiddleware(RequestDelegate next) => _next = next;

        /// <summary>
        /// Обертка с перехватом исключений из следующего обработчика
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context, ILogger<ErrorHandlingMiddleware> logger)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex, logger);
            }
        }

        private static async Task HandleExceptionAsync(HttpContext context, Exception exception, ILogger<ErrorHandlingMiddleware> logger)
        {
            context.Response.ContentType = "application/json";
            context.Response.Headers.TryAdd("Access-Control-Allow-Origin", "*");

            // Контролируемые ошибки
            if (exception is EntityNotFoundException entityNotFoundException)
            {
                var errorId = Guid.NewGuid();
                var errorMessage = $"Entity not found. Error #{errorId}";
                logger.LogError(exception, errorMessage);
                logger.LogError($"EntityNotFoundException: {entityNotFoundException.Message}. Exception as JSON: {JsonConvert.SerializeObject(entityNotFoundException)}");
                var result = JsonConvert.SerializeObject(new
                {
                    StatusCode = StatusCodes.Status404NotFound,
                    Error = entityNotFoundException
                }, _settings);
                context.Response.StatusCode = StatusCodes.Status404NotFound;
                await context.Response.WriteAsync(result, encoding: Encoding.UTF8);
            }
            else if (exception is BusinessLogicException businessLogicException)
            {
                logger.LogError($"BusinessLogicException: {exception.Message}. Exception as JSON: {JsonConvert.SerializeObject(businessLogicException)}");
                var result = JsonConvert.SerializeObject(new
                {
                    StatusCode = StatusCodes.Status400BadRequest,
                    Error = businessLogicException
                }, _settings);
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
                await context.Response.WriteAsync(result, encoding: Encoding.UTF8);
            }
            else
            {
                logger.LogError($"Exception: {exception.Message}. Exception as JSON: {JsonConvert.SerializeObject(exception)}");
                var result = JsonConvert.SerializeObject(new
                {
                    StatusCode = StatusCodes.Status500InternalServerError,
                    Error = exception
                }, _settings);
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                await context.Response.WriteAsync(result, encoding: Encoding.UTF8);
            }
        }
    }
}
