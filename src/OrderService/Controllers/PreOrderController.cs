﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Order.BusinessLogic.Managers.Abstraction;
using Order.BusinessLogic.Models;
using OrderService.Models;

namespace OrderService.Controllers
{
    /// <summary>
    /// PreOrderController
    /// </summary>
    [Route("api/v1/preorder")]
    [ApiController]
    public class PreOrderController : BaseController<PreOrderController>
    {
        private readonly IPreOrderManager _preOrderManager;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="preOrderManager"></param>
        /// <param name="mapper"></param>
        /// <param name="logger"></param>
        public PreOrderController(IPreOrderManager preOrderManager,
            IMapper mapper,
            ILogger<PreOrderController> logger
        )
            : base(logger)
        {
            _preOrderManager = preOrderManager;
            _mapper = mapper;
        }

        /// <summary>
        /// Create pre order
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        public async Task<Guid> PostAsync(PreOrderCreateModel preOrderCreateModel, CancellationToken cancellationToken)
        {
            CheckModel();
            var preOrderCreateDto = _mapper.Map<PreOrderCreateModel, PreOrderCreateDto>(preOrderCreateModel);
            if (User?.Identity?.IsAuthenticated == true)
            {
                preOrderCreateDto.IsAuthorized = true;
                preOrderCreateDto.CreatedUserId = CurrentUserId;
            }
            else
            {
                preOrderCreateDto.IsAuthorized = false;
            }

            var orderCreateResultDto = await _preOrderManager.CreateAsync(preOrderCreateDto, cancellationToken);
            return orderCreateResultDto.Id;

            void CheckModel()
            {
                if (preOrderCreateModel == null)
                {
                    throw new ArgumentNullException(nameof(preOrderCreateModel));
                }
            }
        }

        /// <summary>
        /// Change pre order
        /// </summary>
        [HttpPut("{id}")]
        public async Task<Guid> EditAsync(Guid id, PreOrderChangeModel preOrderChangeModel, CancellationToken cancellationToken)
        {
            CheckModel();
            var preOrderCreateDto = _mapper.Map<PreOrderChangeModel, PreOrderChangeDto>(preOrderChangeModel);
            preOrderCreateDto.UniqId = id;
            if (User?.Identity?.IsAuthenticated == true)
            {
                preOrderCreateDto.IsAuthorized = true;
                preOrderCreateDto.ChangedUserId = CurrentUserId;
            }
            else
            {
                preOrderCreateDto.IsAuthorized = false;
            }

            var preOrderUniqId = await _preOrderManager.ChangeAsync(preOrderCreateDto, cancellationToken);
            return preOrderUniqId;

            void CheckModel()
            {
                if (preOrderChangeModel == null)
                {
                    throw new ArgumentNullException(nameof(preOrderChangeModel));
                }
            }
        }

        /// <summary>
        /// Create pre order
        /// </summary>
        [HttpGet("{id}")]
        public async Task<PreOrderGetResultDto> GetByUniqIdAsync(Guid id, CancellationToken cancellationToken)
        {
            var preOrderGetDto = _mapper.Map<Guid, PreOrderGetDto>(id);
            if (User?.Identity?.IsAuthenticated == true)
            {
                preOrderGetDto.IsAuthorized = true;
                preOrderGetDto.CurrentUserId = CurrentUserId;
            }
            else
            {
                preOrderGetDto.IsAuthorized = false;
            }

            var preOrderGetResultDto = await _preOrderManager.GetByUniqIdAsync(preOrderGetDto, cancellationToken);
            return preOrderGetResultDto;
        }
    }
}