﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Order.BusinessLogic.Managers.Abstraction;
using Order.BusinessLogic.Models;
using OrderService.Models.Location.Requests;
using OrderService.Models.Location.Responses;

namespace OrderService.Controllers
{
    /// <summary>
    /// LocationController
    /// </summary>
    [Route("api/v1/location")]
    [ApiController]
    public class LocationController : BaseController<LocationController>
    {
        private readonly ILocationManager _locationManager;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="locationManager"></param>
        /// <param name="mapper"></param>
        /// <param name="logger"></param>
        public LocationController(ILocationManager locationManager,
            IMapper mapper,
            ILogger<LocationController> logger
        ) :
            base(logger)
        {
            _locationManager = locationManager;
            _mapper = mapper;
        }

        /// <summary>
        /// FindNearby
        /// </summary>
        [HttpPost("findNearby")]
        [Authorize]
        public async Task<ICollection<FindNearbyLocationResponse>> FindNearby(FindNearbyLocationModelRequest request)
        {
            CheckModel();
            var findNearbyLocationDto =
                _mapper.Map<FindNearbyLocationModelRequest, FindNearbyLocationDto>(request);
            findNearbyLocationDto.UserId = CurrentUserId;
            var locationDtos = await _locationManager.FindNearbyAsync(findNearbyLocationDto);
            return locationDtos
                .Select(x => new FindNearbyLocationResponse 
                { 
                    IsAuthorizedUser = x.IsAuthorizedUser,
                    FromLocation = new LocationResponse 
                    {
                        Lat = x.FromLocation.Lat,
                        Lng = x.FromLocation.Lng,
                        Address = x.FromLocation.Address,
                    },
                    ToLocation = new LocationResponse
                    {
                        Lat = x.ToLocation.Lat,
                        Lng = x.ToLocation.Lng,
                        Address = x.ToLocation.Address,
                    },
                    UserId = x.UserId,
                    CreatedDate = x.CreatedDate,
                    SameLocation = x.SameLocation,
                    Distance = x.Distance,
                    HowLong = x.HowLong,                    
                })
                .ToArray();

            void CheckModel()
            {
                if (request == null)
                {
                    throw new ArgumentNullException(nameof(request));
                }
            }
        }
    }
}