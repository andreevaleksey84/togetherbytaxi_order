﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Order.BusinessLogic.Managers.Abstraction;
using Order.BusinessLogic.Models;
using Order.BusinessLogic.Models.Orders;
using OrderService.Models;

namespace OrderService.Controllers;

/// <summary>
/// OrderController
/// </summary>
[Route("api/v1/order")]
[ApiController]
public class OrderController : BaseController<OrderController>
{
    private readonly IOrderManager _orderManager;
    private readonly IMapper _mapper;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="orderManager"></param>
    /// <param name="mapper"></param>
    /// <param name="logger"></param>
    public OrderController(IOrderManager orderManager,
        IMapper mapper,
        ILogger<OrderController> logger
    )
        : base(logger)
    {
        _orderManager = orderManager;
        _mapper = mapper;
    }

    /// <summary>
    /// Get all orders for user
    /// </summary>
    [HttpGet("All")]
    [Authorize]
    public async Task<ICollection<OrderInfoDto>> GetAllAsync(CancellationToken cancellationToken) =>
        await _orderManager.GetAllAsync(new OrderGetAllDto
        {
            CurrentUserId = CurrentUserId
        }, cancellationToken);

    /// <summary>
    /// Create order
    /// </summary>
    [HttpPost]
    [Authorize]
    public async Task<Guid> PostAsync(OrderCreateModel orderCreateModel, CancellationToken cancellationToken)
    {
        CheckModel();
        var orderCreateDto = _mapper.Map<OrderCreateModel, OrderCreateDto>(orderCreateModel);
        orderCreateDto.CurrentUserId = CurrentUserId;

        var orderCreateResultDto = await _orderManager.CreateAsync(orderCreateDto, cancellationToken);
        return orderCreateResultDto.Id;

        void CheckModel()
        {
            if (orderCreateModel == null)
            { 
                throw new ArgumentNullException(nameof(OrderCreateModel));
            }
            if (orderCreateModel.PreOrderUniqId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(OrderCreateModel));
            }
        }
    }

    /// <summary>
    /// Delete order
    /// </summary>
    [HttpDelete("{id}")]
    [Authorize]
    public async Task DeleteAsync(Guid id, CancellationToken cancellationToken) =>
        await _orderManager.DeleteAsync(new OrderDeleteDto
        {
            CurrentUserId = CurrentUserId,
            Id = id
        }, cancellationToken);
}