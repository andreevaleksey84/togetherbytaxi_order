﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Order.BusinessLogic.Managers.Abstraction;
using Order.BusinessLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrderService.Controllers
{
    /// <summary>
    /// OrderController
    /// </summary>
    [Route("api/v1/taxi-list")]
    [ApiController]
    public class TaxiListController : BaseController<TaxiListController>
    {
        private readonly ITaxiListManager _manager;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="manager"><see cref="ITaxiListManager"/></param>
        /// <param name="logger"><see cref="ILogger"/></param>
        public TaxiListController(ITaxiListManager manager,
            ILogger<TaxiListController> logger
        )
            : base(logger)
        {
            _manager = manager;
        }

        /// <summary>
        /// Get taxi list
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ICollection<TaxiListDto>> GetAllAsync()
        {
            var results = await _manager.GetAllAsync();
            return results;
        }
    }
}
