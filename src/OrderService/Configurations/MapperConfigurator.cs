﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using OrderService.MapProfiles;

namespace OrderService.Configurations
{
    /// <summary>
    /// MapperConfigurator
    /// </summary>
    public static class MapperConfigurator
    {
        /// <summary>
        /// ConfigureAutomappers
        /// </summary>
        public static IServiceCollection ConfigureAutomappers(this IServiceCollection services) => services
            .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddBlMapperConfiguration();
                cfg.AddProfile<PreOrderMapProfile>();
                cfg.AddProfile<LocationMapProfile>();
                cfg.AddProfile<OrderMapProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }
    }
}
